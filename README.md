
# Xenia Programming Language

Warning: Work in Progress! The backend is under development.
Check out CONTRIBUTING.md to understand how to navigate the project.

This is the main repository with source code for a novel multi-platform
programming language. The key features are:
* support for multiple platforms via Virtual Machine
* new platforms can be easily added
* JIT-compiler

