
#include "../../utils/unittest.h"

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

#include "../frontend/xtokenizer.h"
#include "../../utils/common.h"

#include "../protocols/xtype.h"

/*
 * Can check internals here - tests will fail if we change them and
 * we will notice it fast.
 */
void check_ok(const char *code, const char *expect)
{
    struct xtoken *toks = NULL;
    struct xtoken_store ts = {0};

    char *f = xtok_tokenize(code, &ts);
    if (f != NULL)
    {
        fprintf(stderr, "xtok_tokenize() failed\n");
        return;
    }

    toks = vector_raw_data(ts.tokens);
    UT_CHECK(xidentifier_t type = xtype_deduct(toks),
            UT_OK(xtype_error(type) == NULL),
            UT_OK(strcmp(xtype2str(type), expect) == 0)
            );
    
    xtoken_store_destroy(&ts, 1);
}

void check_fail(const char *code, const char *expect)
{
    struct xtoken *toks = NULL;
    struct xtoken_store ts = {0};

    char *f = xtok_tokenize(code, &ts);
    if (f != NULL)
    {
        fprintf(stderr, "xtok_tokenize() failed\n");
        return;
    }

    toks = vector_raw_data(ts.tokens);
    UT_CHECK(xidentifier_t type = xtype_deduct(toks),
            UT_EXECUTE(code = xtype_error(type)),
            UT_OK(code != NULL),
            UT_OK(strcmp(code, expect) == 0)
            );

    xtoken_store_destroy(&ts, 1);
}

int main()
{
    MAYBE_UNUSED(XTOK_TYPE_STR);

    ut_enable_color = 1;
    ut_verbose = 0;
    ut_setup();

    check_ok("var x", "v");
    check_ok("func x", "()");

    check_fail("var",  "Expected identifier name after 'var'");
    check_fail("func", "Expected identifier name after 'func'");
    check_fail("5", "Unexpected token, could not parse type");

    ut_finalize();

    return (ut_any_failed ? EXIT_FAILURE : EXIT_SUCCESS);
}

