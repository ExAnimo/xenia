
#include "../../utils/unittest.h"

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

#include "../../utils/common.h"

#include "../frontend/xtokenizer.h"

static void dump(const struct xtoken_store *ts)
{
    size_t n_toks = vector_length(ts->tokens);
    for (size_t i = 0; i < n_toks; i++)
    {
        struct xtoken *tok = vector_at(ts->tokens, i);
        printf("token type: %s\n", XTOK_TYPE_STR[tok->type]);
        switch (tok->type)
        {
            case XTOK_NUMBER: printf("\t%d\n", tok->payload.number); break;
            case XTOK_VAR:    printf("\t'var'\n");                   break;
            case XTOK_FUNC:   printf("\t'func'\n");                  break;
            case XTOK_WHILE:  printf("\t'while'\n");                 break;
            case XTOK_IF:     printf("\t'if'\n");                    break;
            case XTOK_IDENTIFIER:
                printf("\t'%s'\n", tok->payload.symbol);
                break;
            default: /* Nothing */ break;
        }
    }
}

int main()
{
    MAYBE_UNUSED(dump);

    const char *code = NULL;
    struct xtoken *toks = NULL;
    struct xtoken_store ts = {0};

    ut_enable_color = 1;
    ut_verbose = 0;
    ut_setup();

    /* All correct tokens */
    code = "{}(),=<>\n+-*/==<=>=123while var identifier func ! != & && | ||; if";
    UT_CHECK(char *f = xtok_tokenize(code, &ts),
             UT_OK(f == NULL),
             UT_OK(errno == 0),
             UT_OK(vector_length(ts.tokens) == 29 + 1),
             UT_EXECUTE(toks = vector_raw_data(ts.tokens)),
             UT_OK(toks[0].type  == XTOK_BLOCK_OPEN),
             UT_OK(toks[1].type  == XTOK_BLOCK_CLOSE),
             UT_OK(toks[2].type  == XTOK_EXPR_OPEN),
             UT_OK(toks[3].type  == XTOK_EXPR_CLOSE),
             UT_OK(toks[4].type  == XTOK_EXPR_DELIM),
             UT_OK(toks[5].type  == XTOK_TYPE_ASSIGN),
             UT_OK(toks[6].type  == XTOK_LESS),
             UT_OK(toks[7].type  == XTOK_MORE),
             UT_OK(toks[8].type  == XTOK_NEWLINE),
             UT_OK(toks[9].type  == XTOK_PLUS),
             UT_OK(toks[10].type == XTOK_MINUS),
             UT_OK(toks[11].type == XTOK_STAR),
             UT_OK(toks[12].type == XTOK_DIVIDE),
             UT_OK(toks[13].type == XTOK_COMPARE_EQUAL),
             UT_OK(toks[14].type == XTOK_LEQ),
             UT_OK(toks[15].type == XTOK_GEQ),
             UT_OK(toks[16].type == XTOK_NUMBER),
             UT_OK(toks[16].payload.number == 123),
             UT_OK(toks[17].type == XTOK_WHILE),
             UT_OK(toks[18].type == XTOK_VAR),
             UT_OK(toks[19].type == XTOK_IDENTIFIER),
             UT_OK(strcmp(toks[19].payload.symbol, "identifier") == 0),
             UT_OK(toks[20].type == XTOK_FUNC),
             UT_OK(toks[21].type == XTOK_NOT),
             UT_OK(toks[22].type == XTOK_COMPARE_NEQ),
             UT_OK(toks[23].type == XTOK_BIT_AND),
             UT_OK(toks[24].type == XTOK_LOG_AND),
             UT_OK(toks[25].type == XTOK_BIT_OR),
             UT_OK(toks[26].type == XTOK_LOG_OR),
             UT_OK(toks[27].type == XTOK_STMT_DELIM),
             UT_OK(toks[28].type == XTOK_IF)
            );

    // dump(&ts);
    xtoken_store_destroy(&ts, 1);

    /* Bad/weird inputs */
    code = "()$";
    UT_CHECK(char *f = xtok_tokenize(code, &ts),
             UT_OK(errno == 0),
             UT_OK(f != NULL),
             UT_OK(*f == '$')
            );

    code = "";
    UT_CHECK(char *f = xtok_tokenize(code, &ts),
             UT_OK(errno == 0),
             UT_OK(f == NULL),
             UT_OK(vector_length(ts.tokens) == 1 /* XTOK_STOP */),
             UT_CLEANUP(xtoken_store_destroy(&ts, 1))
            );

    code = "===";   /* tokenizer must be greedy */
    UT_CHECK(char *f = xtok_tokenize(code, &ts),
             UT_OK(errno == 0),
             UT_OK(f == NULL),
             UT_OK(vector_length(ts.tokens) == 3 /* XTOK_STOP */),
             UT_EXECUTE(toks = vector_raw_data(ts.tokens)),
             UT_OK(toks[0].type == XTOK_COMPARE_EQUAL),
             UT_OK(toks[1].type == XTOK_TYPE_ASSIGN),
             UT_CLEANUP(xtoken_store_destroy(&ts, 1))
            );

    /* "Fuzzing" */
    code = "(}{  )\n asd while=== var x /*\n \n(*123h1 ;|||&";
    UT_CHECK(char *f = xtok_tokenize(code, &ts),
             UT_OK(errno == 0),
             UT_OK(f == NULL),
             UT_CLEANUP(xtoken_store_destroy(&ts, 1))
            );

    ut_finalize();

    return (ut_any_failed ? EXIT_FAILURE : EXIT_SUCCESS);
}

