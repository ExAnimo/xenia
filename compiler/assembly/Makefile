
CFLAGS :=-Wall -Wextra -Wpedantic -std=c99

srcs_all := $(shell find xasm/ -path '*.c' -type f)
srcs := $(shell find xasm/ -path 'test/*' -prune -o -path '*.c' -type f)
objs := $(patsubst %.c,%.o,$(notdir $(srcs)))
dirs := obj/

bin := xassembly

all: debug

debug release: $(bin)
debug: CFLAGS +=-g#-pg
debug: LDFLAGS +=#-pg
test release: CFLAGS +=-O2 -DNDEBUG

utils := ../../utils/obj/filemem.o ../../utils/obj/common.o \
         ../../utils/obj/htable.o ../../utils/obj/str_arena.o
export CFLAGS
export LDFLAGS
build_utils:
	$(MAKE) -C ../../utils build
$(utils): build_utils

-include .depend
.depend: main.c $(srcs_all)
	$(CC) $(CFLAGS) $(CPPFLAGS) -MM $^ > $@
	sed -i 's/\(.*\):/obj\/\1:/' $@

vpath %.o $(dirs)
$(bin): main.o $(objs) $(utils)
	$(LINK.o) $^ $(LOADLIBES) $(LDLIBS) -o $@

vpath %.c xasm
./obj/%.o: %.c | $(dirs)
	$(COMPILE.c) $(OUTPUT_OPTION) $<

test: ./test/label.testcase $(bin)
	@echo '  Running tests for xasm:'
	@cd test; echo '  Testing labels...'; \
        valgrind -q --leak-check=full --show-leak-kinds=all ./label.testcase
	@echo "  Testing $(bin)...";
	@./$(bin) nonexistent.xasm -o /dev/null 2>/dev/null && \
        echo 'Nonexistent file test failed' || :
	@valgrind -q --leak-check=full --show-leak-kinds=all \
        ./$(bin) nonexistent.xasm 2>/dev/null || :
	@./$(bin) test/full.xasm -o /dev/null 2>/dev/null || \
        echo 'Assembling failed'
	@valgrind -q --leak-check=full --show-leak-kinds=all \
        ./$(bin) test/full.xasm -o /dev/null 2>/dev/null || :

testclean:
	$(RM) ./test/label.testcase

./test/label.testcase: ./test/labeltest.c $(objs) $(utils)
	$(CC) $(CPPFLAGS) $(LDFLAGS) $(CFLAGS) -o $@ $^

clean:
	$(RM) -r $(dirs)
	$(RM) .depend
	$(RM) gmon.out
	$(RM) $(bin)

$(dirs):
	mkdir -p $@

.PHONY: clean testclean

