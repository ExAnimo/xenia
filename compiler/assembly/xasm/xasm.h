
/*
 * File contains declarations for native assembly.
 *
 * There is only one instance of ASM can be used at a time since there is
 * no real reason to maintain several ASM instances in a single process.
 * Thus a single global instance called XASM is used.
 *
 * These API-like functions do not really need access to this structure
 * so its declaration is 'private'.
 */

#ifndef   XASM_H_INCLUDED
#define   XASM_H_INCLUDED

#include <stdio.h>

/*
 * Inits the global XASM structure and associates it with 'infile' and
 * 'outfile'.
 *
 * Returns 0 in case everything is OK, (-1) and sets errno appropriately in
 * case the initialization failed (due to malloc() fail).
 */
int  xasm_init(char *infile, size_t infile_len, FILE *outfile);

/*
 * Gets a single asm command names in the input file and call the handler.
 *
 * Returns 0 in case everything is OK, (-1) in case of illegal command found.
 */
int  xasm_process_cmd();

/*
 * Frees allocated resources.
 */
void xasm_destroy();

/*
 * Fills label info on the first pass and generates the output file on the
 * second pass. Should be called twice. Not expected to be called by a user
 * but still allows for more fine-grained control.
 *
 * NOTE: the function does not increment XASM.pass.
 *
 * See 'xasm_run()' instead.
 *
 * Returns (-1) in case of failure, 0 otherwise.
 */
int  xasm_pass();

/*
 * Generates the complete output file.
 *
 * Returns (-1) in case of failure, 0 otherwise.
 */
int  xasm_run();

#endif // XASM_H_INCLUDED

