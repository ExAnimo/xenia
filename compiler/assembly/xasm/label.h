
/*
 * File contains declarations for label-saving facility for XASM.
 */

#ifndef   XASM_LABELS_H_INCLUDED
#define   XASM_LABELS_H_INCLUDED

#include <stddef.h>
#include "../../../utils/htable.h"
#include "../../../utils/str_arena.h"

/*
 * The main structure: consists of 2 main parts:
 *  1. arena with label names
 *  2. mapping <name, offset> for labels
 *
 * Labels are stored in a 'labels' arena. The htable stores pairs of
 * <name, offset> as <char *, size_t>, where offset is a position of
 * the label in file that we want to store.
 */
struct label_store_t
{
    struct str_arena *labels;
    struct htable    *label_offsets;
};

/*
 * Inits the structure 'ls'.
 *
 * Call label_store_destroy() to release resources.
 *
 * Returns 0 in case everything is OK, (-1) and sets errno appropriately if
 * as error occures.
 */
int     label_store_init(struct label_store_t *ls);

/*
 * Stores the given 'label''s offset 'offset' in the 'ls' if it didn't exist.
 *
 * Returns 0 in case the operation succeeded, (1) in case the label name
 * has already beed stored and (-1) and sets errno appropriately in case
 * of internal error (e.g. malloc() fail).
 */
int     add_label(struct label_store_t *ls, char *label, size_t offset);

/*
 * Returns the offset for the previously stored is 'ls' 'label' or
 * ((size_t)-1) in case the label was not found.
 *
 * TODO: maybe use ssize_t?
 */
size_t  get_label_offset(struct label_store_t *ls, char *label);

/*
 * Frees resources allocated by 'ls'.
 */
void    label_store_destroy(struct label_store_t *ls);

#ifndef   NDEBUG
/*
 * Prints saved labels to stdout.
 */
void    dump_labels(struct label_store_t *ls);
#endif // NDEBUG

#endif // XASM_LABELS_H_INCLUDED
