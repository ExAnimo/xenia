
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "label.h"

/*
 * Transforms char* to htable_data_t.
 */
static inline union htable_data_t to_key(char *k)
{
    return (union htable_data_t){ .vptr = k };
}

/*
 * Taken from http://www.cse.yorku.ca/~oz/hash.html
 */
static size_t hash(union htable_data_t key)
{
    size_t hash = 5381;
    const char *str = (const char*)key.vptr;

    int c;
    while ((c = *str++))
        hash = ((hash << 5) + hash) + c;

    return hash;
}

static int cmp(union htable_data_t key1, union htable_data_t key2)
{
    const char *str1 = (const char*)key1.vptr;
    const char *str2 = (const char*)key2.vptr;
    return strcmp(str1, str2);
}

int label_store_init(struct label_store_t *ls)
{
    assert(ls);

    /* Magic constants are not a problem here */
    ls->labels = str_arena_create(16*1024);
    if (!ls->labels)
        return -1;

    ls->label_offsets = htable_create(1021, hash, cmp);
    if (!ls->label_offsets)
    {
        str_arena_destroy(ls->labels);
        return -1;
    }

    return 0;
}

int add_label(struct label_store_t *ls, char *label, size_t offset)
{
    assert(ls);
    assert(ls->labels);
    assert(ls->label_offsets);
    assert(label);

    /*
     * Inserting offset (-1) to check whether the label with the same name
     * has already been inserted: offset (-1) is illegal.
     *
     * TODO: maybe better use pointers?
     */
    struct htable_entry new_ent = { .key.vptr = label,
                                    .val.uint = (size_t)(-1) };
    struct htable_entry *e = htable_add(ls->label_offsets, &new_ent);
    if (!e) /* Internal failure */
        return -1;
    if ((size_t)e->val.uint != (size_t)(-1))
        return 1;

    /* OK, saving the label, correcting the entry */
    char *stored_label = str_arena_store(ls->labels, label);
    if (!stored_label)
        return -1;

    e->key.vptr = stored_label;
    e->val.uint = offset;

    return 0;
}

size_t get_label_offset(struct label_store_t *ls, char *label)
{
    assert(ls);
    assert(ls->labels);
    assert(ls->label_offsets);
    assert(label);

    struct htable_entry *e = htable_find(ls->label_offsets, to_key(label));
    if (!e)
        return -1;

    return e->val.uint;
}

void label_store_destroy(struct label_store_t *ls)
{
    assert(ls);

    str_arena_destroy(ls->labels);
    htable_destroy(ls->label_offsets);
}

#ifndef   NDEBUG

#include <stdio.h>

void dump_labels(struct label_store_t *ls)
{
    assert(ls);

    fprintf(stderr, "Labels dump:\n");
    htable_rewind(ls->label_offsets);
    struct htable_entry *e = NULL;
    while ((e = htable_iterate(ls->label_offsets)))
        printf("'%s': '%lu'\n", (char*)e->key.vptr, (size_t)e->val.uint);
}
#endif // NDEBUG

