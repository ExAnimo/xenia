
#include <ctype.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include <errno.h>
#include "xasm.h"

/*
 * This file heavily uses the line 'LOG("\n")'. The only purpose is to
 * trace the execution order - LOG by default prints the name of the
 * function that called it.
 */

/*
 * TODO: Use -I in Makefile instead
 */
#include "../../../utils/text.h"
#include "../../../utils/common.h"
#include "../../../utils/log.h"
#include "../../../vm/vm_architecture.h"
#include "../../../vm/exec_img.h"
#include "label.h"

enum xasm_setup
{
    XASM_CODE_MEM = 1024,   /* RIP initial address */
    XASM_MAX_ARGS = 10
};

/*
 * Global struct representing assembly state.
 *
 * The are subtle differences between 'infile' and 'outfile' as file
 * handlers. 'infile' is expected to be read actively and its size and
 * contents are fixed. 'outfile', in contrast, will be primarily
 * written to and its size is hard to determine at first. Thus it would
 * be better to open 'infile' as a string and 'outfile' as a FILE*.
 *
 * 'text_bytes' and 'data_bytes' are used to track amount of bytes written
 * as code or data. It helps to deal with sections.
 *
 * 'current_sym' points to the next symbol to be processed in 'infile'.
 *
 * 'cmd' is a buffer where a command is 'built': parts of the command
 * are gathered in a single string.
 *
 * When the command is parsed, its arguments are stored in 'cmd_operands'
 * in order and 'n_operands' is incremented. The handler for each command
 * should use its arguments.
 *
 * The operand type is a union to allow for future extensions.
 *
 * 'labels' is a facility to store label names found in a file.
 *
 * 'pass' is the number of the pass. The label search depends on it.
 */
static struct xasm
{
    char       *infile;
    size_t      infile_len;
    FILE       *outfile;

    size_t      text_bytes;
    size_t      data_bytes;

    char       *current_sym;

    char        cmd[XASM_MAX_ARGS * 8];

    /* Length can be adjusted if needed */
    union operand_t
    {
        vm_opcode_t num;
        vm_opcode_t reg;
        vm_opcode_t lbl;
    }
    cmd_operands[XASM_MAX_ARGS];

    int         n_operands;

    struct label_store_t    labels;

    int         pass;
}
XASM;

/*
 * A set of special characters used in asm
 */
#define XASM_SYM_COM ';'
#define XASM_SYM_NUM '$'
#define XASM_SYM_REG '%'
#define XASM_SYM_LBL ':'
#define XASM_SYM_MEM_OPEN  '['
#define XASM_SYM_MEM_CLOSE ']'

/*
 * A simple wrapper around standard-library fwrite(). Use it to avoid thinking
 * abount a proper moment to write to a file :)
 *
 * Writes to the file only on the second pass, on the first one always returns
 * succsessfully.
 *
 * Otherwise behaves exaclty like a standard fwrite().
 */
static size_t xasm_fwrite(const void *ptr, size_t size, size_t nmemb,
                          FILE *stream)
{
    if (XASM.pass != 1)
        return fwrite(ptr, size, nmemb, stream);
    
    return nmemb;
}

// do_CMD ==========================================================

/*
 * Writes 'size' bytes pointed to by 'val' to the outfile if
 * XASM.pass != 1.
 *
 * Returns number of bytes written or 0 in case of error. The error
 * message is printed so the caller may return silently.
 */
static inline size_t xasm_file_out(const void *val, size_t size)
{
    assert(val);

    size_t rc = xasm_fwrite(val, size, 1, XASM.outfile);
    if (rc != 1)
    {
        perror("fwrite()");
        return 0;
    }
    return size;
}

/*
 * Generating:
 * 1. do_functions() for every opcode
 * 2. XASM_FUNCNAME_2_IDX mapping: an array of "do_functions"
 * 3. XASM_IDX_2_DOFUNC mapping: an array of do_functions pointers
 * 4. type do_func_type_t fo do_functions
 *
 * The inteded usage is
 * func = XASM_IDX_2_DOFUNC(index_of(XASM_FUNCNAME_2_IDX, "command"));
 */
#include "xasm_do_functions.h"

/*
 * Returns a do_function with name "do_funcname" of NULL if it was not found.
 */
static inline do_func_type_t xasm_get_do_func(const char *do_funcname)
{
    assert(do_funcname);
    LOG("Looking for do-function '%s'\n", do_funcname);

    int idx = index_of(do_funcname, XASM_FUNCNAME_2_IDX);
    if (idx == -1)
        return NULL;

    return XASM_IDX_2_DOFUNC[idx];
}

// Command parsers =================================================
/*
 * CONVENTIONS:
 *  1. After the word was parsed, the XASM.current_sym points to the
 *     beginning of the text word or the end of line.
 *  2. The error description is printed as soon as discovered.
 */

/*
 * Resets the state of the XASM so that a next line can be parsed.
 */
static void xasm_reset_line()
{
    XASM.current_sym = text_skip_blank(XASM.current_sym);
    XASM.cmd[0] = '\0';
    XASM.n_operands = 0;
}

/*
 * Resets XASM for the next pass. Expects 'infile' already set.
 */
static void xasm_reset_full()
{
    LOG("\n");
    XASM.text_bytes = XASM_CODE_MEM;
    XASM.data_bytes = 0;
    XASM.current_sym = XASM.infile;
    xasm_reset_line();
}

/*
 * Prints 'msg' using printf-like format.
 *
 * For now it just prints to stderr but it can be changed later if
 * needed.
 */
static void xasm_print_error(const char *msg, ...)
{
    size_t line, pos;
    text_get_position(XASM.infile, XASM.current_sym, &line, &pos);
    fprintf(stderr, "xasm error: line %zu, char %zu: \n", line, pos);

    va_list ap;
    va_start(ap, msg);
    vfprintf(stderr, msg, ap);
    va_end(ap);

    fprintf(stderr, "\n");
}

/*
 * Consumes the line up to the end. Always successful.
 * Does not read the termitating '\n'.
 */
static inline int xasm_parse_comment()
{
    LOG("\n");
    char *s = XASM.current_sym;
    while (*s && *s != '\n')
        s++;
    
    XASM.current_sym = s;

    return 0;
}

/*
 * Returns (-1) in case of failure, 0 otherwise.
 */
static int xasm_parse_num()
{
    LOG("\n");
    if (*XASM.current_sym == XASM_SYM_NUM)
        XASM.current_sym++;

    vm_opcode_t num = 0;
    int n_chars;
    int rc = sscanf(XASM.current_sym, "%"SCNvmopcode"%n", &num, &n_chars);
    if (rc != 1)
    {
        xasm_print_error("Could not read number");
        return -1;
    }

    LOG("Got %"PRIvmopcode"\n", num);
    XASM.cmd_operands[XASM.n_operands++].num = num;
    XASM.current_sym = text_skip_blank(XASM.current_sym + n_chars);

    strcat(XASM.cmd, "_NUM");

    return 0;
}

/*
 * Returns (-1) in case the register name is unknown, 0 otherwise.
 */
static int xasm_parse_reg()
{
    LOG("\n");
    if (*XASM.current_sym == XASM_SYM_REG)
        XASM.current_sym++;

    char name[4] = { '\0' };
    size_t name_len = 0;
    // Register's name can touch closing bracket
    while (isalnum(*XASM.current_sym) && name_len < sizeof(name))
        name[name_len++] = *XASM.current_sym++;
    name[name_len] = '\0';

    text_to_upper(name);
    LOG("Got '%s'\n", name);

    int reg = index_of(name, VM_REGNAMES);
    if (reg < 0)
    {
        xasm_print_error("register name not found: '%s'", name);
        return -1;
    }

    XASM.cmd_operands[XASM.n_operands++].reg = reg;
    XASM.current_sym = text_skip_blank(text_get_word_end(XASM.current_sym));
    strcat(XASM.cmd, "_REG");

    return 0;
}

/*
 * Returns (-1) in case the register name is unknown or braces are
 * not paired, 0 otherwise.
 *
 * TODO: Make expressions in brackets
 */
static int xasm_parse_mem()
{
    LOG("\n");
    if (*XASM.current_sym == XASM_SYM_MEM_OPEN)
        XASM.current_sym++;

    XASM.current_sym = text_skip_blank(XASM.current_sym);
    strcat(XASM.cmd, "_MEM");

    int rc;
    switch (*XASM.current_sym)
    {
        case XASM_SYM_NUM:  rc = xasm_parse_num();  break;
        case XASM_SYM_REG:  rc = xasm_parse_reg();  break;
        default:
            xasm_print_error("unexpected symbol");
            return -1;
    }
    if (rc != 0)
        return -1;
    
    if (*XASM.current_sym != XASM_SYM_MEM_CLOSE)
    {
        xasm_print_error("could not find memory closing bracket");
        return -1;
    }
    XASM.current_sym++; // Skipping ']'
    XASM.current_sym = text_skip_blank(text_get_word_end(XASM.current_sym));

    return 0;
}

/*
 * Stores an uppercased instruction under cursor to XASM.cmd.
 * Returns (-1) in case of failure, 0 otherwise.
 */
static int xasm_parse_instruction()
{
    LOG("\n");
    char cmd[32] = { '\0' };
    int rc = sscanf(XASM.current_sym, "%32s", cmd);
    if (rc != 1)
    {
        xasm_print_error("could not read an instruction");
        return -1;
    }

    size_t cmdlen = strlen(cmd);
    if (cmdlen <= 1)
    {
        xasm_print_error("command '%s' is too short", cmd);
        return -1;
    }

    text_to_upper(cmd);
    LOG("Got '%s'\n", cmd);

    XASM.current_sym = text_skip_blank(XASM.current_sym + cmdlen);
    strcat(XASM.cmd, cmd);

    return 0;
}

/*
 * Parses a label reference.
 *
 * Function calculations depend on the pass number:
 *  1. first pass: always returns 0;
 *  2. second pass: if the label was not found, return an error.
 *
 * During the first pass some labels cannot be know at the reference time,
 * e.g. in a forward jump, and during the second jump all the labels'
 * offsets must be known - thus an unexpected label is an error.
 *
 * Returns offset if the offset was found, (size_t)-1 otherwise.
 */
static size_t xasm_get_label(char *label)
{
    assert(label);
    LOG("Looking for '%s'\n", label);

    if (XASM.pass == 1)
    {
        LOG("First pass, succsessfull return\n");
        return 0;
    }

    size_t offset = get_label_offset(&XASM.labels, label);
    if (offset == (size_t)-1)
    {
        xasm_print_error("unknown label name '%s'", label);
        return (size_t)-1;
    }

    LOG("Got %zu\n", offset);
    return offset;
}

/*
 * Resolves the label name to the offset.
 */
static int xasm_parse_label()
{
    LOG("\n");
    char label[32] = { '\0' };
    int rc = sscanf(XASM.current_sym, "%32s", label);
    if (rc != 1)
    {
        xasm_print_error("could not read a label");
        return -1;
    }

    size_t offset = xasm_get_label(label);
    if (offset == (size_t)-1)
        return -1;

    XASM.cmd_operands[XASM.n_operands++].lbl = offset;
    XASM.current_sym = text_skip_blank(text_get_word_end(XASM.current_sym));
    strcat(XASM.cmd, "_LBL");

    return 0;
}

/*
 * NOTE: the function does not follow the general return convention.
 * Pay attention.
 *
 * Parses an argument of a command.
 *
 * Returns whether the line has ended (1), 0 in case of success and
 * (-1) if an error occured.
 */
static int xasm_parse_arg()
{
    LOG("\n");
    int (*callback)() = NULL;
    switch (*XASM.current_sym)
    {
        case XASM_SYM_NUM:      callback = xasm_parse_num;      break;
        case XASM_SYM_MEM_OPEN: callback = xasm_parse_mem;      break;
        case XASM_SYM_REG:      callback = xasm_parse_reg;      break;
        case XASM_SYM_COM:      callback = xasm_parse_comment;  break;
        case '\n':              return 1;
        default  :              callback = xasm_parse_label;    break;
    }
    int rc = callback();
    if (rc < 0)
        return -1;
    return *XASM.current_sym == '\n';
}

/*
 * Tries to store the label and prints error in case of failure.
 * The caller should assume that all the error mesages have been printed
 * and just do its cleanup and return.
 *
 * Returns 0 in case of success, (-1) in case of failure.
 */
static int xasm_add_label(char *label, size_t offset)
{
    LOG("\n");
    assert(label);

    int rc = add_label(&XASM.labels, label, offset);
    switch (rc)
    {
        case  0:
            if (XASM.pass == 1)
            {
                LOG("Label '%s' added on the first pass\n", label);
                return  0;
            }
            else
            {
                xasm_print_error("Label '%s' first seen on the second pass",
                    label);
                return -1;
            }
        case  1:
            if (XASM.pass == 1)
            {
                xasm_print_error("Redefinition of label '%s'", label);
                return -1;
            }
            else
            {
                LOG("Label '%s' already defined on the second pass\n", label);
                return  0;
            }
        case -1:
            perror("add_label() internal fail");
            return -1;
        default:
            fprintf(stderr, "add_label() uknown return code %d\n", rc);
            return -1;
    }
}

/*
 * Parses a line looking like
 *  cmd [arg1 [arg2 [...]]]
 * or
 *  ; comment
 *
 * Returns (-1) in case of failure, 0 otherwise.
 */
static int xasm_parse_cmd()
{
    LOG("\n");
    if (*XASM.current_sym == XASM_SYM_COM)
        return xasm_parse_comment();

    int rc = xasm_parse_instruction();
    if (rc != 0)
        return -1;

    int n_args = 0;
    while ((rc = xasm_parse_arg()) == 0)   /* Till the end of line */
        if (n_args++ > XASM_MAX_ARGS)
        {
            xasm_print_error("Too many args\n");
            return -1;
        }
    
    return (rc == 1) ? 0 : -1;
}

/*
 * Parses a line looking like
 *  [label:] cmd [arg1 [arg2 [...]]]
 * or
 *  label: [; comment]
 * or
 *  ; [comment]
 *
 * Returns (-1) in case of fail, 0 otherwise.
 */
static int xasm_parse_line()
{
    LOG("\n");
    xasm_reset_line();

    // Empty line
    if (*XASM.current_sym == '\n')
        return 0;

    char buf[32] = { '\0' };
    int rc = sscanf(XASM.current_sym, "%32s", buf);
    if (rc != 1)
    {
        xasm_print_error("could not read an instruction or label");
        return -1;
    }

    if (buf[0] == XASM_SYM_COM)
        return xasm_parse_comment();

    size_t cmdlen = strlen(buf);
    if (cmdlen <= 1)
    {
        xasm_print_error("command '%s' is too short", buf);
        return -1;
    }

    /* Optional label */
    if (buf[cmdlen - 1] == XASM_SYM_LBL)
    {
        LOG("Line started with label '%s'\n", buf);
        buf[cmdlen - 1] = '\0';
        rc = xasm_add_label(buf, XASM.text_bytes);
        if (rc != 0)
            return -1;

        XASM.current_sym = text_skip_blank(XASM.current_sym + cmdlen);
    }

    /* End of line or a command */
    return (*XASM.current_sym == '\n') ? 0 : xasm_parse_cmd();
}

// 'API' ===========================================================

int xasm_init(char *infile, size_t infile_len, FILE *outfile)
{
    UNUSED(VM_FLAGNAMES);

    assert(infile);
    assert(outfile);
    LOG("\n");

    XASM.infile = infile;
    XASM.infile_len = infile_len;
    XASM.outfile = outfile;
    xasm_reset_full();

    XASM.pass = 1;

    return label_store_init(&XASM.labels);
}

void xasm_destroy()
{
    LOG("\n");
    label_store_destroy(&XASM.labels);
}

int  xasm_process_cmd()
{
    LOG("Next command: =============================================\n");
    int rc = xasm_parse_line();
    if (rc != 0)
        return -1;

    XASM.current_sym++;         /* Skipping '\n' */

    if (XASM.cmd[0] == '\0')    /* Label or comment or empty line */
    {
        LOG("Just a comment, returning succsessfully\n");
        return 0;
    }

    do_func_type_t handler = xasm_get_do_func(XASM.cmd);
    if (!handler)
    {
        xasm_print_error("Unknown command signature: %s\n", XASM.cmd);
        return -1;
    }
    LOG("Calling a handler\n");
    rc = handler();

    return rc;
}

int  xasm_pass()
{
    LOG("\n");
    // 1. Reset for the pass
    xasm_reset_full();

    // 2. Reserve place for header
    struct vm_exec_img_hdr hdr = { 0 };
    size_t wr = xasm_file_out(&hdr, sizeof(hdr));
    if (wr == 0)
        return -1;

    // 3. Write the data
    int rc = 0;
    while (*XASM.current_sym && rc == 0)
        rc = xasm_process_cmd();
    if (rc != 0)
        return -1;

    // 4. Re-fill the header with actual info
    rewind(XASM.outfile);
    hdr = (struct vm_exec_img_hdr){
        .magic = VM_MAGIC,
        .version = VM_VERSION,

        .code_mem = XASM_CODE_MEM,
        .code_beg = sizeof(hdr),
        .code_len = (XASM.text_bytes - XASM_CODE_MEM) * sizeof(vm_opcode_t)
    };
    wr = xasm_file_out(&hdr, sizeof(hdr));
    if (wr == 0)
        return -1;

    LOG("Written header:\n"
        ".magic    = 0x%"PRIx32"\n.version  = %u\n"
        ".code_mem = %zu\n.code_beg = %zu\n.code_len = %zu\n",
        hdr.magic, hdr.version, hdr.code_mem, hdr.code_beg, hdr.code_len);
    return 0;
}

int  xasm_run()
{
    int rc;

    LOG("First  pass (XASM.pass = %d)\n", XASM.pass);
    rc = xasm_pass();
    if (rc != 0)
        return -1;

    LOG_CALL(dump_labels(&XASM.labels));

    XASM.pass++;
    LOG("Second pass (XASM.pass = %d)\n", XASM.pass);
    rc = xasm_pass();
    if (rc != 0)
        return -1;

    return 0;
}

