
/*
 * Asm driver: parses keys and calls an apropriate assembly:
 * xasm, as, wasm, etc, according to the file extension (or
 * the key, probably)
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

/*
 * TODO: Non-portable
 */
#include <getopt.h>

#include "../../utils/filemem.h"
#include "xasm/xasm.h"

static void help(FILE *out)
{
    assert(out);

    fprintf(out,
        "Options:\n"
        "   -o  output filename\n"
        "   -h  print this message and exit\n"
        );
}

static void usage(FILE *out, const char *name)
{
    assert(out);
    assert(name);

    fprintf(out, "usage: %s [-h] file.xasm [-o output.xex]\n", name);
}

int main(int argc, char **argv)
{
    const char *outfile_name = "out.xex";
    int ret = EXIT_SUCCESS;
    int rc;

    int opt;
    opterr = 0; // Suppressing error output
    while ((opt = getopt(argc, argv, "ho:")) != -1)
        switch (opt)
        {
            case 'o':
                outfile_name = optarg;
                break;
            case 'h':
                help(stdout);
                return EXIT_SUCCESS;
            default:
                usage(stderr, argv[0]);
                return EXIT_FAILURE;
        }
    if (optind != (argc - 1))
    {
        usage(stderr, argv[0]);
        return EXIT_FAILURE;
    }

    // Finally, all is OK
    size_t insize;
    char *infile = utils_file_to_memory(argv[optind], "r", &insize);
    if (!infile)
    {
        fprintf(stderr, "utils_file_to_memory() failed\n");
        return EXIT_FAILURE;
    }

    FILE *outfile = fopen(outfile_name, "wb");
    if (!outfile)
    {
        ret = EXIT_FAILURE;
        goto err_quit_0;
    }

    rc = xasm_init(infile, insize, outfile);
    if (rc != 0)
    {
        ret = EXIT_FAILURE;
        goto err_quit_1;
    }

    rc = xasm_run();
    if (rc != 0)
    {
        fprintf(stderr, "xasm_run() failed\n");
        ret = EXIT_FAILURE;
    }

    xasm_destroy();

err_quit_1:
    fclose(outfile);
err_quit_0:
    utils_memory_to_file(infile);

    return ret;
}

