
#include "../../../utils/unittest.h"

#include "../xasm/label.h"
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

int main()
{
    ut_enable_color = 1;
    ut_verbose = 0;
    ut_setup();

    /* 1. Tests for label store */

    struct label_store_t ls;
    UT_CHECK(int rc = label_store_init(&ls),
             UT_OK(rc == 0)
            );

    // Hope for a realloc...
    for (volatile int i = 0; i < 2000; i++)
    {
        char buf[128] = { '\0' };
        sprintf(buf, "long_label_num_%d", i);
        UT_CHECK(int rc = add_label(&ls, buf, (size_t)i),
                UT_OK(rc == 0)
                );
    }
    for (volatile int i = 0; i < 2000; i++)
    {
        char buf[128] = { '\0' };
        sprintf(buf, "long_label_num_%d", i);
        UT_CHECK(size_t off = get_label_offset(&ls, buf),
                UT_OK(off == (size_t)i)
                );
    }

    label_store_destroy(&ls);

    /* 2. Tests for xasm */
    /* TODO */

    ut_finalize();

    return (ut_any_failed ? EXIT_FAILURE : EXIT_SUCCESS);
}

