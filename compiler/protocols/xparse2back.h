
/*
 * File contains structure declarations that enable communications
 * between xparser and compiler's backend.
 */

#ifndef   XPARSE2BACK_H_INCLUDED
#define   XPARSE2BACK_H_INCLUDED

#include "xtype.h"

/*
 * A macro is used to generate both values for enum and their names.
 * Use of a different file for this purpose looks more like an overkill,
 * so place new token types here and they will be expanded properly.
 */
#define XNODE_TYPE_MACRO                        \
    EXPAND(XNODE_PLUS)                          \
    EXPAND(XNODE_MINUS)                         \
    EXPAND(XNODE_MULT)                          \
    EXPAND(XNODE_DIVIDE)                        \
    EXPAND(XNODE_NOT)                           \
                                                \
    EXPAND(XNODE_LESS)                          \
    EXPAND(XNODE_MORE)                          \
    EXPAND(XNODE_LEQ)                           \
    EXPAND(XNODE_GEQ)                           \
    EXPAND(XNODE_COMPARE_EQUAL)                 \
    EXPAND(XNODE_COMPARE_NEQ)                   \
    EXPAND(XNODE_LOG_AND)                       \
    EXPAND(XNODE_BIN_AND)                       \
    EXPAND(XNODE_LOG_OR)                        \
    EXPAND(XNODE_BIN_OR)                        \
                                                \
    EXPAND(XNODE_LOCAL)                         \
    EXPAND(XNODE_GLOBAL)                        \
    EXPAND(XNODE_ASSIGN)                        \
                                                \
    EXPAND(XNODE_CONST)                         \
                                                \
    /* Service nodes */                         \
    EXPAND(XNODE_EXPR)                          \
    EXPAND(XNODE_CALL)                          \
                                                \
    /* Sentinel token */                        \
    EXPAND(XNODE_STOP)


#define EXPAND(name) name, 
enum xnode_type
{
    XNODE_TYPE_MACRO /* Expansion */

    XNODE_N_TYPES
};
#undef EXPAND

#define EXPAND(name) #name,
static const char *XNODE_TYPE_STR[] =
{
    XNODE_TYPE_MACRO /* Expansion */

    "XNODE_N_TYPES"
};
#undef EXPAND

#undef XTOK_TYPE_MACRO


// The main struct =====================================================

/*
 * 'payload' and 'identifier_type' are assigned and used only with
 * variables and functions now.
 */
union xnode_payload
{
    const char *symbol; // For globals, calls
    unsigned    id;     // For locals

    /*
     * TODO: consts can be of different type - use sth else. Like
     * union { intmax_t, uintmax_t, void * }
     *
     */
    int         number; // For const
};

/*
 * The syntax tree node
 */
struct xnode
{
    union xnode_payload payload;

    xidentifier_t       identifier_type;

    /* expr, global, local, const, if, while, etc. */
    enum xnode_type     type;
};

#endif // XPARSE2BACK_H_INCLUDED

