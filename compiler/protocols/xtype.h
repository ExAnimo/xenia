
/*
 * The file contains definitions for Xenia identifier types.
 *
 * The compiler uses a special 'encoding language' to encode and store
 * the types of identifiers (inspired by "The practive of Programming",
 * Kernighan and Pike).
 *
 * To hide the complexity and incapsulate all the type-related stuff,
 * it is encouraged to only use facilities defined here when dealing
 * with the types.
 *
 * The internal representation of the type should be considered hidden,
 * although it is visible in the file. It is made visible to allow for
 * static allocations and embedding in structs, but it can be changed
 * without any notification.
 */

#ifndef   XTYPE_H_INCLUDED
#define   XTYPE_H_INCLUDED

#include <assert.h>
// TODO: change to gcc -I
#include "../../utils/common.h"
#include "xtok2xparse.h"

/*
 * Current version uses a string to encode the type.
 *
 * The type functions can allocate extra chars, located in the beginning
 * of the char array to store additional info, like an indication of failure.
 *
 * Currently, 1 char is always reserved in this way. If it is '!', then the
 * string contains the error message, and a correct type otherwise.
 *
 * The encoding is simple:
 *  1. "v" = 'variable'
 *  2. "(vv)v" = func, that receives 2 vars and returns a var
 * Composite types are to be added.
 */
typedef char *xidentifier_t;


/*
 * Funcs to 'hide' the xidentifier underlying type: client code should
 * use them instead of manual checking.
 */
static inline int xtype_is_set(xidentifier_t t)
{
    return t != NULL && t[-1] != '!';
}

/* Returns an error if type encodes it, NULL otherwise */
static inline const char *xtype_error(xidentifier_t t)
{
    return (t && t[-1] == '!') ? t : NULL;
}

static inline char *xtype2str(xidentifier_t t)
{
    assert(t);
    MAYBE_UNUSED(XTOK_TYPE_STR);

    return t;
}

/*
 * Deducts the type of the identifier. Currently supports:
 *  1. function type 'deduction'
 *  2. variable type 'deduction' - now it is always 'var', but should be
 *     expanded to accomodate for cases like "x := 5 + f(7.0)"
 *
 * 'toks' is expected to point to:
 *  1. 'func' token
 *  2. 'var' token
 *
 * WARNING: the returned xidentifier is NOT guaranteed to be allocated in
 * any way, can be a const string or a static array.
 * If you are going to store it, make a copy.
 *
 * Returns a resulting xidentifier. If type_error(type_deduct(toks))
 * is non-NULL, the function failed to find the type and the error is
 * encoded in the return result.
 */
xidentifier_t xtype_deduct(struct xtoken *toks);

#endif // XTYPE_H_INCLUDED

