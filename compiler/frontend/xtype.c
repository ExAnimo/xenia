
#include <assert.h>
#include "../protocols/xtype.h"

/*
 * Use underscore as a success identifier.
 */
#define OK(str)     (("_" str) + 1)
#define FAIL(str)   (("!" str) + 1)

static xidentifier_t xtype_parse_var(struct xtoken *toks)
{
    assert(toks);

    if (toks[1].type != XTOK_IDENTIFIER)
        return FAIL("Expected identifier name after 'var'");

    return OK("v");
}

/*
 * TODO: actually, calculate the type. Now returns sth like
 * void f(void);
 */
static xidentifier_t xtype_parse_func(struct xtoken *toks)
{
    assert(toks);

    if (toks[1].type != XTOK_IDENTIFIER)
        return FAIL("Expected identifier name after 'func'");

    return OK("()");
}

xidentifier_t xtype_deduct(struct xtoken *toks)
{
    assert(toks);

    switch (toks->type)
    {
        case XTOK_VAR:      return xtype_parse_var(toks);
        case XTOK_FUNC:     return xtype_parse_func(toks);
        default: return FAIL("Unexpected token, could not parse type");
    }

    assert(0 && "Cannot be here");
    return FAIL("Out of switch, impossible condition, type deduction failed");
}

