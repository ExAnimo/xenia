
/*
 * File contains declarations for xparser.
 */

#ifndef   XPARSER_H_INCLUDED
#define   XPARSER_H_INCLUDED

#include "../protocols/xtok2xparse.h"
#include "xtokenizer.h"

/*
 * Inits xparser with token store 'ts', that must be obtained from the
 * xtokenizer.
 *
 * Returns (-1) in case of failure and sets errno appropriately, 0 otherwise.
 * If the function returned (-1), calling any other xparser functions is UB.
 */
int xparse_init(struct xtoken_store ts);

/*
 * Performs the recursive descend, constructs the syntax tree. Call only
 * after a call to xparse_init().
 *
 * Returns a parsed tree or a NULL in case of failure. If a failure occured,
 * prints the diagnostic message to stderr.
 */
struct bintree *xparse_parse();

/*
 * Frees the allocated storage. If 'with_data' is not 0, deletes data from
 * the 'ts', used to initialize the xparser, as well.
 */
void xparse_destory(int with_data);

/*
 * Dumps the tree, rooted at 'root', to stdout in DOT format.
 */
void xparser_dump_tree(struct bintree *root);

#endif // XPARSER_H_INCLUDED

