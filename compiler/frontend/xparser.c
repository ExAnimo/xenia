
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "../protocols/xparse2back.h"
#include "../protocols/xtype.h"
#include "../../utils/bintree.h"
#include "../../utils/common.h"
#include "../../utils/text.h"
#include "xparser.h"

/*
 * A 'temporal' struct to save the local and global identifiers.
 * Their name and type are only needed during parsing, backend
 * only has to know about external identifiers.
 */
struct identifier
{
    const char         *name;
    xidentifier_t       type;
    unsigned            id;     // 0 for globals
};
static int is_global(const struct identifier *i)
{
    assert(i);
    return i->id == 0;
}

/*
 * We can allocate tree nodes and corresponding data as a whole.
 */
struct node_full
{
    struct bintree  tree;
    struct xnode    payload;
};

/*
 * A global structure: only one instance of the parser is expected to
 * be run at a time, so there is no need to overload the functions'
 * signature with it.
 */
struct
{
    /* Saving it to delete all the data later */
    struct xtoken_store      ts;

    /* Points somewhere in 'ts' */
    const struct xtoken     *cur_token;

    /* 'cur_node' points to the first empty node in 'nodes' */
    struct node_full        *nodes;
    struct node_full        *cur_node;

    /*
     * TODO: must be slow... Make a symbol table.
     * A symtab with list of identifiers with same names, order = depth
     *
     * It is big enought, but can be changed to be dynamically allocated
     * if needed later.
     */
    struct identifier        identifiers[8192];

    /*
     * Used to implement the scope, controlled by { and }. The algorithm
     * mimics rbp/rsp registers of x86_64: 'scope' always points to the first
     * empty identifier in 'identifiers', 'prev_scope' is used to save and
     * and restore the scope.
     */
    struct identifier       *scope;
    struct identifier       *prev_scope;

    /* An easy check to catch invalid calls before init() */
    int                      inited;
}
XPARSER;


int xparse_init(struct xtoken_store ts)
{
    assert(ts.tokens);

    XPARSER.ts = ts;
    XPARSER.cur_token = vector_raw_data(ts.tokens);
    XPARSER.scope = XPARSER.identifiers;
    XPARSER.prev_scope = XPARSER.scope;

    size_t n_nodes = vector_length(ts.tokens);
    struct node_full *tree = malloc(sizeof(*tree) * n_nodes);
    if (!tree)
        return -1;
    XPARSER.nodes = tree;
    XPARSER.cur_node = XPARSER.nodes;

    XPARSER.inited = 1;
    return 0;
}

void xparse_destory(int with_data)
{
    MAYBE_UNUSED(XTOK_TYPE_STR);
    MAYBE_UNUSED(XNODE_TYPE_STR);

    assert(XPARSER.inited);

    xtoken_store_destroy(&XPARSER.ts, with_data);
    free(XPARSER.nodes);
}

// The recursive descend functions =================================
/*
 * Helpers
 */

static int xnode_print_node(union bintree_data_t val, FILE* outfile,
                            void *supplementary_args)
{
    (void)supplementary_args;

    struct xnode *node = (struct xnode *)val.vptr;

    fprintf(outfile, "%s", XNODE_TYPE_STR[node->type]);
    switch (node->type)
    {
        case XNODE_LOCAL:
            fprintf(outfile, "| %d", node->payload.id);
            break;
        case XNODE_GLOBAL:
            fprintf(outfile, "| %s", node->payload.symbol);
            break;
        case XNODE_CONST:
            fprintf(outfile, "| %d", node->payload.number);
            break;
        default:    /* empty */
            break;
    }

    return 0;
}


static void xparse_print_error(const char *msg, ...)
{
    /* The first sym of a file resides in the sentinel node */
    size_t n_tokens = vector_length(XPARSER.ts.tokens);
    const struct xtoken *tok = vector_at(XPARSER.ts.tokens, n_tokens - 1);
    const char *file = tok->file_position;

    /* Allegedely a node, that caused the fail */
    tok = XPARSER.cur_token;

    size_t line, pos;
    text_get_position(file, tok->file_position, &line, &pos);
    fprintf(stderr, "xenia error: line %zu, char %zu: \n", line, pos);

    va_list ap;
    va_start(ap, msg);
    vfprintf(stderr, msg, ap);
    va_end(ap);

    fprintf(stderr, "\n");
}

/*
 * Saves 'node' in an array of prepared tree nodes and returns the
 * prepared tree node. Always successful.
 */
static struct bintree *xparse_set_next_node(const struct node_full *node)
{
    /* Enough space must have been allocated in advance */
    assert(XPARSER.cur_node <
        XPARSER.nodes + vector_length(XPARSER.ts.tokens));
    assert(node);

    *XPARSER.cur_node = *node;
    struct bintree *tree = &XPARSER.cur_node->tree;
    tree->val.vptr = &XPARSER.cur_node->payload;
    XPARSER.cur_node++;

    return tree;
}

/*
 * Returns pointer to the first found identifier with 'name'. Searches
 * in LIFO order. Return NULL in case the identifier was not found.
 */
static struct identifier *find_identifier(const char *name)
{
    assert(name);

    /* It may point behind the array, but it is safe */
    struct identifier *cur = XPARSER.scope;
    while (--cur >= XPARSER.identifiers)
        if (strcmp(cur->name, name) == 0)
            return cur;

    return NULL;
}

static enum xnode_type node_type_of(const struct identifier *i)
{
    assert(i);
    return is_global(i) ? XNODE_GLOBAL : XNODE_LOCAL;
}

static union xnode_payload node_payload_of(const struct identifier *i)
{
    assert(i);

    union xnode_payload p = { .id = 0 };
    if (is_global(i))
        p.symbol = i->name;
    else
        p.id = i->id;
    return p;
}

/*
 * Fills 'node' with info from 'i' and returns a pointer to prepared 'node'.
 * Neigher 'node' nor 'i' whould be NULL.
 */
static void identifier_to_node(struct node_full *node,
                               const struct identifier *i)
{
    assert(node);
    assert(i);

    node->payload.type = node_type_of(i);
    node->payload.identifier_type = i->type;
    node->payload.payload = node_payload_of(i);
}

/*
 * Consumes everithing up to the last parentheses, INCLUDING it. The function
 * does not follow the general rule, but only due to the fact that
 * is called only in one place and can be considered a simple shortcut.
 * Enforcing the general rule would only cause untrivial code.
 *
 * TODO: now functions can only have empty list
 */
static struct bintree *get_func_arglist()
{
    /* At least assume it the list was set correctly and
       contains XTOK_STOP at the end... */
    const struct xtoken *tok;
    while ((tok = XPARSER.cur_token++)->type != XTOK_EXPR_CLOSE)
    {
        /* Only empty parentheses are OK */
        assert(tok->type == XTOK_NEWLINE);
    }

    return NULL;
}

/*
 * The grammar mimics the one of C.
 *
 * Every function reads next token in an input stream and constructs a tree.
 * If a function cannot read a tree, it "backs off" and DOES NOT consume
 * the unexpected token.
 *
 * The functions already have long name, so we will step aside and remove
 * a lib prefix.
 *
 * Each function returns a pointer to the constructed subtree.
 */

/* The pattern is used too often here: do not use it with dynamic memory */
#define RET_ON_NULL(p)      \
    do {                    \
        if (!(p))           \
            return NULL;    \
    } while (0)

static struct bintree *get_primary_expr();
static struct bintree *get_postfix_expr();
static struct bintree *get_unary_expr();
static struct bintree *get_mult_expr();
static struct bintree *get_add_expr();
static struct bintree *get_rel_expr();
static struct bintree *get_log_and_expr();
static struct bintree *get_log_or_expr();
static struct bintree *get_assign_expr();
static struct bintree *get_expression();

/* Following functions are less straightforward */
static struct bintree *get_statement();
static struct bintree *get_control();
static struct bintree *get_func();
static struct bintree *get_var();

/*
 * Makes some sort of a maping from xtokens to xnodes. Actually, is one
 * of the most difficult parts of the descend in this respect.
 */
static struct bintree *get_primary_expr()
{
    struct node_full node;

    struct identifier *i = NULL;
    struct bintree *e = NULL;
    const struct xtoken *tok = XPARSER.cur_token++;
    switch (tok->type)
    {
        case XTOK_IDENTIFIER:
            i = find_identifier(tok->payload.symbol);
            RET_ON_NULL(i);
            identifier_to_node(&node, i);
            node.tree.left = node.tree.right = NULL;
            break;

        case XTOK_NUMBER:
            node.payload.type = XNODE_CONST;
            node.payload.payload.number = tok->payload.number;
            node.tree.left = node.tree.right = NULL;
            break;

        case XTOK_EXPR_OPEN:
            /* '(' is already skipped */
            e = get_expression();
            RET_ON_NULL(e);
            if (XPARSER.cur_token->type != XTOK_EXPR_CLOSE)
            {
                /* There must be a closing brace */
                /* TODO: verbose error message */
                assert(0 && "Invalid char in get_primary_expr()");
                return NULL;
            }
            XPARSER.cur_token++;
            return e;

        default:    /* An error or really an expression */
            /* TODO: verbose error message */
            assert(0 && "Invalid char in get_primary_expr()");
            return NULL;;
    }

    return xparse_set_next_node(&node);
}

static struct bintree *get_postfix_expr()
{
    struct bintree *e = get_primary_expr();
    RET_ON_NULL(e);

    /* TODO: ++, --, [] */
    while (XPARSER.cur_token->type == XTOK_EXPR_OPEN)
    {
        /* Skip '(', ')' is skipped in get_func_arglist() */
        XPARSER.cur_token++;
        struct bintree *args = get_func_arglist();

        struct node_full node;
        node.payload.type = XNODE_CALL;
        node.tree.left = e;
        node.tree.right = args;

        e = xparse_set_next_node(&node);
    }

    return e;
}

static int is_unary_op(enum xtok_type type)
{
    return  type == XTOK_PLUS   ||
            type == XTOK_MINUS  ||
            type == XTOK_NOT;
}

static struct bintree *get_unary_expr()
{
    struct bintree *e = NULL;
    struct bintree **indirect = &e;
    while (is_unary_op(XPARSER.cur_token->type))
    {
        struct node_full node;
        node.tree.right = NULL;
        switch (XPARSER.cur_token->type)
        {
            case XTOK_PLUS:  node.payload.type = XNODE_PLUS;  break;
            case XTOK_MINUS: node.payload.type = XNODE_MINUS; break;
            case XTOK_NOT:   node.payload.type = XNODE_NOT;   break;
            default:
                xparse_print_error("Unsupported unary operation: %s\n",
                                   XTOK_TYPE_STR[XPARSER.cur_token->type]);
                return NULL;
        }

        *indirect = xparse_set_next_node(&node);
        indirect = &(*indirect)->left;

        XPARSER.cur_token++;
    }

    *indirect = get_postfix_expr();
    RET_ON_NULL(*indirect); /* There MUST be a node */

    return e;
}

/* Should be in sync with get_mult_expr() */
static int is_mult_op(enum xtok_type type)
{
    return  type == XTOK_STAR   ||
            type == XTOK_DIVIDE;
}

static struct bintree *get_mult_expr()
{
    struct bintree *e = get_unary_expr();
    RET_ON_NULL(e);
    
    while (is_mult_op(XPARSER.cur_token->type))
    {
        struct node_full node;
        switch (XPARSER.cur_token->type)
        {
            case XTOK_STAR:   node.payload.type = XNODE_MULT;   break;
            case XTOK_DIVIDE: node.payload.type = XNODE_DIVIDE; break;
            default:
                xparse_print_error("Unsupported mult operation: %s\n",
                                   XTOK_TYPE_STR[XPARSER.cur_token->type]);
                return NULL;
        }
        XPARSER.cur_token++;

        struct bintree *term = get_unary_expr();
        RET_ON_NULL(term);

        node.tree.right = e;
        node.tree.left  = term;
        e = xparse_set_next_node(&node);
    }

    return e;
}

/* Should be in sync with get_add_expr() */
static int is_add_op(enum xtok_type type)
{
    return  type == XTOK_PLUS   ||
            type == XTOK_MINUS;
}

static struct bintree *get_add_expr()
{
    struct bintree *e = get_mult_expr();
    RET_ON_NULL(e);

    while (is_add_op(XPARSER.cur_token->type))
    {
        struct node_full node;
        switch (XPARSER.cur_token->type)
        {
            case XTOK_PLUS:  node.payload.type = XNODE_PLUS;  break;
            case XTOK_MINUS: node.payload.type = XNODE_MINUS; break;
            default:
                xparse_print_error("Unsupported add operation: %s\n",
                                   XTOK_TYPE_STR[XPARSER.cur_token->type]);
                return NULL;
        }
        XPARSER.cur_token++;

        struct bintree *term = get_mult_expr();
        RET_ON_NULL(term);

        node.tree.right = e;
        node.tree.left  = term;
        e = xparse_set_next_node(&node);
    }

    return e;
}

/* Should be in sync with get_rel_expr() */
static int is_rel_op(enum xtok_type type)
{
    return  type == XTOK_LESS           ||
            type == XTOK_MORE           ||
            type == XTOK_LEQ            ||
            type == XTOK_GEQ            ||
            type == XTOK_COMPARE_EQUAL  ||
            type == XTOK_COMPARE_NEQ;
}

static struct bintree *get_rel_expr()
{
    struct bintree *e = get_add_expr();
    RET_ON_NULL(e);

    while (is_rel_op(XPARSER.cur_token->type))
    {
        struct node_full node;
        switch (XPARSER.cur_token->type)
        {
            case XTOK_LESS:  node.payload.type = XNODE_LESS;  break;
            case XTOK_MORE:  node.payload.type = XNODE_MORE;  break;
            case XTOK_LEQ:   node.payload.type = XNODE_LEQ;   break;
            case XTOK_GEQ:   node.payload.type = XNODE_GEQ;   break;
            case XTOK_COMPARE_EQUAL:
                node.payload.type = XNODE_COMPARE_EQUAL;
                break;
            case XTOK_COMPARE_NEQ:
                node.payload.type = XNODE_COMPARE_NEQ;
                break;
            default:
                xparse_print_error("Unsupported rel operation: %s\n",
                                   XTOK_TYPE_STR[XPARSER.cur_token->type]);
                return NULL;
        }
        XPARSER.cur_token++;

        struct bintree *term = get_add_expr();
        RET_ON_NULL(term);

        node.tree.right = e;
        node.tree.left  = term;
        e = xparse_set_next_node(&node);
    }

    return e;
}

static struct bintree *get_log_and_expr()
{
    struct bintree *e = get_rel_expr();
    RET_ON_NULL(e);

    while (XPARSER.cur_token->type == XTOK_LOG_AND)
    {
        struct node_full node;
        node.payload.type = XNODE_LOG_AND;

        XPARSER.cur_token++;
        struct bintree *term = get_rel_expr();
        RET_ON_NULL(term);

        node.tree.left  = term;
        node.tree.right = e;
        e = xparse_set_next_node(&node);
    }

    return e;
}

static struct bintree *get_log_or_expr()
{
    struct bintree *e = get_log_and_expr();
    RET_ON_NULL(e);

    while (XPARSER.cur_token->type == XTOK_LOG_OR)
    {
        struct node_full node;
        node.payload.type = XNODE_LOG_OR;

        XPARSER.cur_token++;
        struct bintree *term = get_log_and_expr();
        RET_ON_NULL(term);

        node.tree.right = e;
        node.tree.left  = term;
        e = xparse_set_next_node(&node);
    }

    return e;
}

/*
 * TODO: parser internals exposed now: fix it?
 */
static struct bintree *get_assign_expr()
{
    struct node_full *save_free_nodes = XPARSER.cur_node;
    const struct xtoken *save_tok = XPARSER.cur_token;

    struct bintree *e = get_unary_expr();
    RET_ON_NULL(e);

    struct bintree **indirect = &e;
    /*
     * Due to the checkpoint, we have to deal with all the side effects
     * first, so the order of operations in a loop diverges from the one
     * seen in previous functions.
     */
    while (XPARSER.cur_token->type == XTOK_TYPE_ASSIGN)
    {
        struct node_full node;
        node.payload.type = XNODE_ASSIGN;

        XPARSER.cur_token++;

        node.tree.left = *indirect;    /* 'right' will be set later */
        *indirect = xparse_set_next_node(&node);
        indirect = &(*indirect)->right;

        // Checkpoint
        save_free_nodes = XPARSER.cur_node;
        save_tok = XPARSER.cur_token;

        struct bintree *term = get_unary_expr();
        *indirect = term;
    }

    // Fallback: reading log-or
    XPARSER.cur_node = save_free_nodes;
    XPARSER.cur_token = save_tok;

    *indirect = get_log_or_expr();
    RET_ON_NULL(*indirect);

    return e;
}

static struct bintree *get_expression()
{
    struct bintree *e = get_assign_expr();
    RET_ON_NULL(e); /* TODO: no no-op ops are permitted... */

    struct bintree **indirect = &e;
    while (XPARSER.cur_token->type == XTOK_EXPR_DELIM)
    {
        struct node_full node;
        node.payload.type = XNODE_EXPR;

        XPARSER.cur_token++;

        struct bintree *term = get_assign_expr();
        RET_ON_NULL(term);

        node.tree.left  = term;
        node.tree.right = *indirect;
        *indirect = xparse_set_next_node(&node);
        indirect = &(*indirect)->left;
    }

    return e;
}

static struct bintree *get_statement()
{
    struct bintree *e = NULL;
    switch (XPARSER.cur_token->type)
    {
        case XTOK_VAR:
            // return get_var();
            break;
        case XTOK_FUNC:
            // return get_func();
            break;
        case XTOK_IF:
        case XTOK_WHILE:
            // return get_control();
            break;
        default:
            /* Can be NULL for empty statement */
            e = get_expression();
            if (XPARSER.cur_token->type == XTOK_STMT_DELIM)
                XPARSER.cur_token++;
            return e;
            break;
    }

    xparse_print_error("Unknown statement type, but did not catch in default:"
                       " %s\n", XTOK_TYPE_STR[XPARSER.cur_token->type]);
    return NULL;
}

static struct bintree *get_control()
{
    return NULL;
}

static struct bintree *get_func()
{
    return NULL;
}

static struct bintree *get_var()
{
    return NULL;
}


struct bintree *xparse_parse()
{
    assert(XPARSER.inited);

    struct identifier i = { "hih", "_v", 0 };
    XPARSER.identifiers[0] = i;
    XPARSER.scope++;

    // return get_primary_expr();
    // return get_postfix_expr();
    // return get_unary_expr();
    // return get_mult_expr();
    // return get_add_expr();
    // return get_rel_expr();
    // return get_log_and_expr();
    // return get_log_or_expr();
    // return get_assign_expr();
    return get_expression();
}

#undef RET_ON_NULL

// Printing/debug   ================================================

void xparser_dump_tree(struct bintree *root)
{
    bintree_fdump_dot(root, xnode_print_node, stdout, NULL);
    fprintf(stdout, "\n");
}

