
/*
 * File contains declarations for xtokenizer.
 */

#ifndef   XTOKENIZER_H_INCLUDED
#define   XTOKENIZER_H_INCLUDED

#include "../protocols/xtok2xparse.h"

/*
 * Tokenizes file 'file' and places the resulting struct xtok_tokenize
 * to the 'ts'. In case tokenizing finished successfully, 'payload' of
 * all of the tokens is set appropriately:
 *  1. identifiers have payload.symbol populated;
 *  2. numbers have payload.number populated;
 *  3. other token types have payload value unspecified;
 *
 * The caller should call xtoken_store_destroy(ts, 1) later to free the
 * resources.
 *
 * Returns NULL in case of success. There are two sources of errors:
 *  1. a symbol was not recognized and could not be parsed;
 *  2. an internal error occured (e.g. malloc() fail).
 * To distinguish between the two types of error, check errno. If
 * errno == 0, then a pointer to the invalid symbol was returned.
 * If errno != 0, it is set appropriately.
 *
 * Function sets errno to 0 in case of successful completion.
 */
char *xtok_tokenize(const char *file, struct xtoken_store *tok_store);

/*
 * Frees 'ts''s internal vectors. If 'with_data' is non-null, their
 * underlying arrays are deleted, too, otherwise caller is responsive for
 * saving them before the call to xtoken_store_destroy() and freeing them.
 */
void xtoken_store_destroy(struct xtoken_store *ts, int with_data);

#endif // XTOKENIZER_H_INCLUDED

