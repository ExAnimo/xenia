
#include <assert.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

#include "xtokenizer.h"
// TODO: change to gcc -I
#include "../../utils/text.h"
#include "../../utils/vector.h"
#include "../../utils/log.h"
#include "../../utils/common.h"


// Token store helpers =================================================

/*
 * Inits the token store 'ts'.
 *
 * Returns 0 in case of success, (-1) and sets errno appropriately otherwise.
 */
static int xtoken_store_init(struct xtoken_store *ts)
{
    assert(ts);

    ts->tokens = vector_create(sizeof(struct xtoken), 64, 0);
    if (!ts->tokens)
        return -1;

    ts->symbols = str_arena_create(0);  // Default size
    if (!ts->symbols)
    {
        vector_destroy(ts->tokens, 1);
        return -1;
    }
    LOG("Inited xtoken store, len(tokens) = %zu\n", vector_length(ts->tokens));

    return 0;
}

// Tokenizer helpers ===================================================

/*
 * Appends XTOK_STOP.
 *
 * Returns (-1) in case of failure, 0 otherwise.
 */
static int xtok_finalize(const char *beg, struct xtoken_store *ts)
{
    assert(ts);
    LOG("\n");

    struct xtoken tok = { .payload.number = -1  /* Any number */,
                          .file_position = beg,
                          .type = XTOK_STOP };
    int ret = vector_append(ts->tokens, &tok);
    if (ret != 0)
        return -1;

    return 0;
}

/* Easier than including a math library */
static inline size_t min(size_t a, size_t b)
{
    return (a < b) ? a : b;
}

/*
 * Parses and stores a number.
 *
 * Returns NULL in case of failure, pointer to next-past-the-last
 * symbol of the number read.
 */
static const char *tokenize_num(const char *beg, struct xtoken_store *ts)
{
    assert(beg);
    assert(ts);
    LOG("\n");

    int num;
    int n_chars;
    int ret = sscanf(beg, "%d%n", &num, &n_chars);
    if (ret != 1)
        return NULL;
    LOG("Got number: %d\n", num);

    struct xtoken tok = { .payload.number = num,
                          .file_position = beg,
                          .type = XTOK_NUMBER };

    ret = vector_append(ts->tokens, &tok);
    if (ret != 0)
        return NULL;

    return beg + n_chars;
}

/*
 * Returns the type of the word 'token'.
 */
enum xtok_type get_word_type(const char *token)
{
    assert(token);
    assert(strlen(token) > 0);
    LOG("\n");

    #define CHECK(keyword, token_type)          \
        do {                                    \
            if (strcmp(token, (keyword)) == 0)  \
                return (token_type);            \
        } while (0)

    CHECK("var", XTOK_VAR);
    CHECK("func", XTOK_FUNC);
    CHECK("while", XTOK_WHILE);
    CHECK("if", XTOK_IF);

    #undef CHECK

    return XTOK_IDENTIFIER;
}

/*
 * Parses and stores a "word" (identifiers/keywords).
 *
 * Returns NULL in case of failure, pointer to next-past-the-last
 * symbol of the number read.
 */
static const char *tokenize_word(const char *beg, struct xtoken_store *ts)
{
    assert(beg);
    assert(ts);
    LOG("\n");

    assert(isalnum(*beg) || *beg == '_');

    /*
     * Currently words are copied to a buffer to be compared with keywords.
     * Probably it is not efficient, but very simple.
     *
     * TODO: profile the code and see whether copying is really expensive.
     */
    /* A rather permissive value */
    char token[256] = { '\0' };
    const char *end = text_get_word_end(beg);

    size_t token_len = min(end - beg, sizeof(token) - 1);
    strncpy(token, beg, token_len);
    token[token_len] = '\0';
    LOG("Got token name: '%s'\n", token);

    char *stored_sym = NULL;
    enum xtok_type type = get_word_type(token);
    if (type == XTOK_IDENTIFIER)
    {
        stored_sym = str_arena_store(ts->symbols, token);
        if (!stored_sym)
            return NULL;
    }
    LOG("Got token's type: %s\n", XTOK_TYPE_STR[type]);

    struct xtoken tok = { .payload.symbol = stored_sym,
                          .file_position = beg,
                          .type = type };

    int ret = vector_append(ts->tokens, &tok);
    if (ret != 0)
        return NULL;

    return end;
}

/*
 * Parses and stores symbols that are not numbers and "words".
 *
 * Returns NULL in case of failure, pointer to next-past-the-last
 * symbol of the number read.
 */
static const char *tokenize_punct(const char *beg, struct xtoken_store *ts)
{
    assert(beg);
    assert(ts);
    LOG("\n");

    int n_chars = 1;
    enum xtok_type type = XTOK_STOP;

    switch (*beg)
    {
        case '\n':  type = XTOK_NEWLINE;        break;
        case '{':   type = XTOK_BLOCK_OPEN;     break;
        case '}':   type = XTOK_BLOCK_CLOSE;    break;
        case '(':   type = XTOK_EXPR_OPEN;      break;
        case ')':   type = XTOK_EXPR_CLOSE;     break;
        case ';':   type = XTOK_STMT_DELIM;     break;
        case ',':   type = XTOK_EXPR_DELIM;     break;

        // TODO: maybe, *= versions, too?
        case '+':   type = XTOK_PLUS;           break;
        case '-':   type = XTOK_MINUS;          break;
        case '*':   type = XTOK_STAR;           break;
        case '/':   type = XTOK_DIVIDE;         break;

        /*
         * Next tokens may consist of 1 or 2 chars: check out next symbol.
         *
         * The code is very simple but repetitve, and it is much easier to
         * write it down manually than to develop an elaborate define
         * system or a function set. If more complex symbols will be added in
         * the future, a function call may be appropriate, but now it would
         * be overkill.
         *
         * The simplicity and repeatability of the code are the reasons why
         * I choose different codestyle here: compressing the code like this
         * actually improves readability.
         */
        case '<':
            if (*(beg + 1) == '=')  { type = XTOK_LEQ;      n_chars++;  }
            else                    { type = XTOK_LESS;                 }
            break;
        case '>':
            if (*(beg + 1) == '=')  { type = XTOK_GEQ;      n_chars++;  }
            else                    { type = XTOK_MORE;                 }
            break;
        case '=':
            if (*(beg + 1) == '=')  { type = XTOK_COMPARE_EQUAL; n_chars++; }
            else                    { type = XTOK_TYPE_ASSIGN;              }
            break;
        case '!':
            if (*(beg + 1) == '=')  { type = XTOK_COMPARE_NEQ;   n_chars++; }
            else                    { type = XTOK_NOT;                      }
            break;
        case '&':
            if (*(beg + 1) == '&')  { type = XTOK_LOG_AND;  n_chars++;  }
            else                    { type = XTOK_BIT_AND;              }
            break;
        case '|':
            if (*(beg + 1) == '|')  { type = XTOK_LOG_OR;   n_chars++;  }
            else                    { type = XTOK_BIT_OR;               }
            break;

        default:    return NULL;                break;
    }
    LOG("Got token's type: %s\n", XTOK_TYPE_STR[type]);

    struct xtoken tok = { .payload.number = -1 /* Any number */,
                          .file_position = beg,
                          .type = type };

    int ret = vector_append(ts->tokens, &tok);
    if (ret != 0)
        return NULL;

    return beg + n_chars;
}

// Tokenizer's API =====================================================

char *xtok_tokenize(const char *file, struct xtoken_store *token_store)
{
    assert(file);
    assert(token_store);
    LOG("\n");

    const char *file_save = file;
    errno = 0;

    // Init local token store
    struct xtoken_store ts = {0};
    int ret = xtoken_store_init(&ts);
    if (ret != 0)
        return (char*)file;

    while (*file)
    {
        LOG("Tokenizing... =====================\n");
        const char *token_end = NULL;

        /* Choosing an action according to the first symbol's char */
        if (isdigit(*file))
            token_end = tokenize_num(file, &ts);
        else
        if (*file == '_' || isalpha(*file))
            token_end = tokenize_word(file, &ts);
        else
            token_end = tokenize_punct(file, &ts);

        if (!token_end)
        {
            xtoken_store_destroy(&ts, 1);
            return (char*)file;
        }

        file = text_skip_blank(token_end);
    }

    ret = xtok_finalize(file_save, &ts);
    if (ret != 0)
    {
        xtoken_store_destroy(&ts, 1);
        return (char*)file;
    }

    *token_store = ts;
    LOG("Got %zu tokens\n", vector_length(ts.tokens));
    return NULL;
}

void xtoken_store_destroy(struct xtoken_store *ts, int with_data)
{
    // Suppress warning
    MAYBE_UNUSED(XTOK_TYPE_STR);

    assert(ts);
    LOG("\n");

    vector_destroy(ts->tokens, with_data);
    str_arena_destroy(ts->symbols);
}

