
This document tries to give a rather complete introduction to the code.
It will likely fall behind the development branch, not it should not lag
much.

# Directory structure

To facilitate navigation and contribute to maintaining logical structure in
the future, the structure of the compiler is denoted here. Purpose of each
of the directory/file mentioned is explained further.

```
|
|_ compiler/
|   |_ frontend/
|   |_ backend/
|   |_ protocols/
|   |_ assembly/
|       |_ xasm/
|       |_ wasm/
|       |_ main.c
|
|_ utils/
```

1. `compiler/`
This directory contains all files, used to build a compiler. It generally
should not depend on any outer dir in a project, except for the `utils/` one.

Inner folders represent different parts of a compiler, that communicate with
each other using structures, whose definitions can be found in `protocols/`.

`assembly/` stands out a bit, since it is a part of compiler only because it
is the last step of code preparation. Assemblers for different architectures
can be just supplied as precompiled binaries.

1. `frontend/`
This part contains files, that are responsible for primary text processing,
i.e. tokenizing and building a syntax tree.

1. `backend/`
Contains a part of the compiler, that receives the syntax tree and genenrates
assembly code.

1. `protocols/`
Different parts of the compiler have to interact in a somewhat uniform way. This
directory serves to 'rendezvous' somewhere in a uniform way.

