
/*
 * Compiler driver. Calls necessary frontend, optimizer, backend and asm
 * to produce code for the requested architecture.
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "frontend/xtokenizer.h"
#include "frontend/xparser.h"
#include "../utils/common.h"

int main()
{
    MAYBE_UNUSED(XTOK_TYPE_STR);

//    printf("\n\n\n\tTOKENIZER\n");

    struct xtoken_store ts = {0};
    const char *code =
        "{}();<>\n+-*/== <= >= 123\n324 ()   1\n\n\n  2 {}"
        "while var var  x xvar whilez while  123";

    code = "1 || +--hih()() / 5 != 90 ++ hih * -hih() && 789";
    // code = "5 && 6 || 0 && !7() || 9()";
    // code = "1 = 2 = 3 = 4 = hih+-123*321";
    // code = "(4 + 5) * (6 + 7); ((((5))))";
    code = xtok_tokenize(code, &ts);
    if (code != NULL)
    {
        if (errno != 0)
            perror("xtok_tokenize()");
        else
            fprintf(stderr, "xtok_tokenize() failed on '%s'\n", code);
        return EXIT_FAILURE;
    }

//    printf("Ok. Printing tokens:\n");
//
//    size_t n_toks = vector_length(ts.tokens);
//    for (size_t i = 0; i < n_toks; i++)
//    {
//        struct xtoken *tok = vector_at(ts.tokens, i);
//        printf("token type: %s\n", XTOK_TYPE_STR[tok->type]);
//        switch (tok->type)
//        {
//            case XTOK_NUMBER: printf("\t%d\n", tok->payload.number); break;
//            case XTOK_VAR:    printf("\t'var'\n");                   break;
//            case XTOK_WHILE:  printf("\t'while'\n");                 break;
//            case XTOK_IDENTIFIER:
//                printf("\t'%s'\n", tok->payload.symbol);
//                break;
//            default: /* Nothing */ break;
//        }
//    }
//
//    printf("\n\n\n\tPARSER\n");

    int ret = xparse_init(ts);
    if (ret == -1)
    {
        perror("xparse_init() failed");
        xtoken_store_destroy(&ts, 1);
        return EXIT_FAILURE;
    }

    struct bintree *bt = xparse_parse();

//    printf("bt = %p\n", (void*)bt);
    xparser_dump_tree(bt);

    xparse_destory(1);

    return 0;
}

