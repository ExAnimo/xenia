
[TOC]

# Project Description

## Breif description

The Xenia language supports compiling into several architectures, addressing
the M+N problem in a classical way: compiler's frontend first transforms the
code in an Abstract Syntax Tree (AST), which is passed throught on optimizer
that returns a prunned version of the AST. Next an approprite backend steps
in: to port the language to a new architecture you only need to create a new
backend.

It order to allow the code to run on almost every architecture a VM is used
(C compiler support only needed). To compile the code into a "native"
assembler (xasm) is used.

## Project layout

The directories and files form the following tree. Only the main files
are listed here, test/ directories, \*.md or Makefiles are ignored.

```
|
|_ vm/
|   |_ native_asm.h
|   |_ exec_img.h
|   |_ vm_architecture.h
|   |_ vm.{c,h}
|   |_ main.c
|
|_ compiler/
|   |_ frontend/
|   |_ backend/
|   |_ protocols/
|   |_ assembly/
|   |   |_ xasm/
|   |   |_ wasm/
|   |   |_ main.c
|   |_ protocols/
|
|_ utils/
```

## Motivation

The aformentioned directory structure corresponds to the modules used to
transform the .xen code to the executable.

As it was already stated in the first subsection, the compiler works as
follows:

1. The compiler's frontend transforms the source code for Xenia programming
language into an AST
1. An optimizer pruns the AST. It only should perform 'obvious' optimizations
such as no-op statements eliminations
1. A requested backend is selected. It can perform further platform-specific
optimizations and must produce an assembly file
1. An approprite assembly is called to transform the asm file to the
executable/object file for a given architecture

## Code generation

Adding new VM instructions requiers simultaneous updating of the code of
the VM itself as well as the assembly's code. To mitigare maintainance
problems this code is generated automatically based on the supplied
description. Since the language is expected to be easily used on any platform
supporting the C language, the C preprocessor is used as a code generator.

# Codestyle Guides

These guides are heavily inspired by the book "The Practice of Programming"
by B. Kernighan and R. Pike.

## General issues

This section covers the topics primarily related to the code formatting.

- Use 4 spaces to indent code, not tabs
- Indent all the nest structures
- Avoid placing the code on the same line with a loop or an if-statement:
It can hide the code from test-coverage tools such as `gcov`
- Place each statement on a separate line. Do not chain them with a comma
- Use curly braces only to surround the nested constructions consisting
of more than one line
- Place `if` on the next line after `else` to maintain constant offset of
the condition
- Add a single whitespace between the flow control statements and a succinct
bracket
- Do not place whitespaces between a function name and a bracket
- Always place a curly bracket on a separate line and use the same indent as
used in the current context
- Always put the closing curly bracket under the corresponding opening one
- Keep lines' length under 80 characters
- Indent the second line if the statement is too long to fit in one line

An exaple:

```C
void example_function(void)
{
    if (cond)
        stmt;
    else
    if (another_cond)
    {
        one_more;
        two_more;
    }
    else
    if (almost_the_last_cond)
    {
        /* Even with a comment */
        another;
    }
    else
    if (the_last_cond)
    {
        or_when_the_statement =
            is_really_long_so_that_it_does_not_fit_in_a_line(unfortunately);
    }
    else
    {
        /* It is still an open issue how is it better to use brackets
         *  else-part...
         */
    }
}
```

## Project issues

This section focuses on the topics related to the project itself.

- Start each file with a brief description of its contents
- Declare functions `static` as much as possible to avoid name collisions
- Prefix 'export' function names with some sort of a module they belong to.
'static' functions are free to use the same manual name mangling though it
is not enforced
- Place a description of a function in front of its declaration. Such a
description should include at least a description of the function's actions,
its arguments and return value
- Start and end each file with an empty line

# Testing

All the code whould be tested. At least 2 types of tests are employed:
1. unittests
1. intergration tests

Unittests are performed with unittest.h framework in the `utils/` library.
Integration tests should be created manually since they require specific
knowledge about the tested program.

Both unittests and intergration tests should use valgrind to check for memory
leaks.

# Logging

A lightweight homemade logging facility (utils/log.h) is used in a project.
Logs are intended to be used primarily during development and debugging of
the utilies, so there is no need to support complex logging, output formatting,
syslog and other close-to-monitoring things.

Probably, a call graph builder can be created later or just a standard one can
be used.

# Branching model

2 primary branches are used within this project: main, which contains stable
releases, and develop, which accumulates changes from other branches. For
example, branches 'utils' and 'eco' are long-term branches, dedicated to
common utilities and language's ecosystem (readme's, syntax highlighting, etc.)
development, respectively.

