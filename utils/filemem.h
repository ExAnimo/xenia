
/*
 * File containing functions that allow for working with files as with
 * memory buffers efficiently.
 *
 * It is also expected to gather and hide the most non-portable things.
 *
 * NOT THREAD-SAFE!
 */

#ifndef   UTILS_H_INCLUDED
#define   UTILS_H_INCLUDED

#include <stddef.h>

/*
 * Places contents of the file 'filename' in memory. Call
 * utils_file_from_memory() after usage to free the resources.
 * Argument 'mode' can take values "r", "w", "rw" for read, write,
 * read + write modes. Underlying file should support requested
 * open mode.
 * Length of the opened file is placed to 'file_len' if it is not NULL. Its
 * value is undefined if the function returned error.
 *
 * func_result[filelen] == '\0' -- you can assume the resulting string
 * is null-terminated.
 *
 * Returns pointer to file contents or NULL in case of failure and
 * sets errno appropriately or
 *  EBADSLT - too much opened files;
 *  EBADRQC - wrong mode;
 *  EBADFD  - requested operation on a file with lenght == 0.
 */
void *utils_file_to_memory(const char *filename, const char *mode,
                           size_t *filelen);

/*
 * Writes the memory contents from 'filemem' obtained via
 * utils_file_to_memory() back to the underlying file and frees allocated
 * resources.
 *
 * Returns (-1) in case the 'filemem' was not allocated by
 * utils_file_to_memory(), 0 in case of success.
 */
int utils_memory_to_file(const void *filemem);

#ifndef NDEBUG

/*
 * Prints RECORDS to stdout.
 */
void utils_dump_records();

#endif

#endif // UTILS_H_INCLUDED

