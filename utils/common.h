
/*
 * File with common utility functions.
 */

#ifndef   COMMON_UTILITIES_H_INCLUDED
#define   COMMON_UTILITIES_H_INCLUDED

/*
 * Dealing with unused-warning.
 */
#define UNUSED(x) (void)(x)
#define MAYBE_UNUSED(x) UNUSED(x)

#define _STR(x) #x
#define STR(x) _STR(x)

/*
 * Finds index of 'name' in array 'in'. If not found, returns (-1).
 *
 * Last element in 'in' must be NULL.
 */
int index_of(const char *name, const char *in[]);

#endif // COMMON_UTILITIES_H_INCLUDED

