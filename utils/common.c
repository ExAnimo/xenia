
#include <assert.h>
#include <string.h>

#include "common.h"

int index_of(const char *name, const char *in[])
{
    assert(name);
    assert(in);

    for (int i = 0; in[i]; i++)
        if (strcmp(in[i], name) == 0)
            return i;
    return -1;
}

