
#include "../unittest.h"

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

#include "../filemem.h"

int main()
{
    FILE *fout = fopen("tmp/file", "w");
    fprintf(fout, "Hello, Elliot");
    fclose(fout);
    fout = fopen("tmp/empty", "w");
    fclose(fout);

    size_t len;

    ut_enable_color = 1;
    ut_verbose = 0;
    ut_setup();

    /* File test */
    UT_CHECK(char *f = utils_file_to_memory("doesnt_exist", "r", &len),
             UT_OK(f == NULL),
             UT_OK(errno != 0)
            );
    UT_CHECK(char *f = utils_file_to_memory("tmp/file", "wrong", &len),
             UT_OK(f == NULL),
             UT_OK(errno == EBADRQC)
            );
    UT_CHECK(char *f = utils_file_to_memory("tmp/empty", "r", &len),
             UT_OK(f == NULL),
             UT_OK(errno == EBADFD)
            );

    UT_CHECK(char *f = utils_file_to_memory("tmp/file", "wr", &len),
             UT_OK(f != NULL),
             UT_OK(len == 13),
             UT_OK(strcmp(f, "Hello, Elliot") == 0),
             UT_EXECUTE(f[0] = 'J'),
             UT_CLEANUP(utils_memory_to_file(f))    // Not so good...
            );
    UT_CHECK(char *f = utils_file_to_memory("tmp/file", "rw", &len),
             UT_OK(f != NULL),
             UT_OK(len == 13),
             UT_OK(f[0] == 'J'),
             UT_OK(strcmp(f, "Jello, Elliot") == 0),
             UT_EXECUTE(f[0] = 'H'),
             UT_CLEANUP(utils_memory_to_file(f)),   // Not so good...
             UT_OK(f[len] == '\0')
            );

    UT_CHECK(int rc = utils_memory_to_file(NULL),
             UT_OK(rc == 0)
            );

    for (volatile int i = 0; i < 1000; i++)
    {
        UT_CHECK(char *f = utils_file_to_memory("tmp/file", "rw", &len),
                 UT_OK(f != NULL),
                 UT_OK(len == 13),
                 UT_OK(strcmp(f, "Hello, Elliot") == 0),
                 UT_CLEANUP(utils_memory_to_file(f))    // Not so good...
                );
        if (ut_test_failed)
            printf("failed on %d'th iteration\n", i);
    }

    ut_finalize();

    return (ut_any_failed ? EXIT_FAILURE : EXIT_SUCCESS);
}

