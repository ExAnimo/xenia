
#include "../unittest.h"

#include <string.h>
#include <stdlib.h>

#include "../str_arena.h"

volatile const char *out;

static void check()
{
    const char *strs[] =
    {
        "asd", "fgh", "jkl",
        "long_string",
        "a_very_very_very_long_string",
        "and even moe long-long string, wow\n",
        "a", "b", "b", "b", "b"
    };
    volatile const char *out_strs[sizeof(strs)/sizeof(strs[0])];

    struct str_arena *arena = str_arena_create(10);
    for (volatile size_t i = 0; i < sizeof(strs)/sizeof(strs[0]); i++)
    {
        UT_CHECK(out = str_arena_store(arena, strs[i]),
                 UT_OK(out != NULL),
                 UT_OK(strcmp((const char*)out, strs[i]) == 0)
                );
        out_strs[i] = out;
    }
    for (volatile size_t i = 0; i < sizeof(strs)/sizeof(strs[0]); i++)
    {
        UT_CHECK(out = out_strs[i],
                 UT_OK(out != NULL),
                 UT_OK(strcmp((const char*)out, strs[i]) == 0)
                );
    }

    str_arena_destroy(arena);
}

static void stress_test()
{
    const char *s = "a long string to cause several reallocs";
    struct str_arena *arena = str_arena_create(10);
    for (volatile size_t i = 0; i < 10000; i++)
    {
        UT_CHECK(char *out = str_arena_store(arena, s),
                 UT_OK(out != NULL),
                 UT_OK(strcmp(out, s) == 0)
                );
    }

    str_arena_destroy(arena);
}

int main()
{
    ut_enable_color = 1;
    ut_verbose = 0;
    ut_setup();

    UT_CHECK(struct str_arena *arena = str_arena_create(10),
             UT_OK(arena != NULL),
             UT_CLEANUP(str_arena_destroy(arena))
            );

    volatile char *s;
    UT_CHECK(struct str_arena *arena = str_arena_create(10),
             UT_OK(arena != NULL),
             UT_CLEANUP(str_arena_destroy(arena)),
             UT_EXECUTE(s = str_arena_store(arena, "hi")),
             UT_OK(s != NULL),
             UT_EXECUTE(s = str_arena_store(arena, "hi1")),
             UT_OK(s != NULL),
             UT_EXECUTE(s = str_arena_store(arena, "hi2")),
             UT_OK(s != NULL),
             UT_EXECUTE(s = str_arena_store(arena, "a_new_long_string\n")),
             UT_OK(s != NULL),
             UT_EXECUTE(s = str_arena_store(arena, "another_long_string\n")),
             UT_OK(s != NULL),
             UT_EXECUTE(s = str_arena_store(arena, "long_string1")),
             UT_OK(s != NULL),
             UT_EXECUTE(s = str_arena_store(arena, "long_string2")),
             UT_OK(s != NULL)
            );

    check();
    stress_test();

    ut_finalize();

    return (ut_any_failed ? EXIT_FAILURE : EXIT_SUCCESS);
}

