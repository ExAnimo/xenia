
#include "../unittest.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "../bintree.h"

static int print_visitor(struct bintree *node, void *arg)
{
    (void)arg;
    printf(" %d\n", (int)node->val.sint);

    return 0;
}

static int ok_visitor(struct bintree *node, void *arg)
{
    (void)arg;
    (void)node;
    return 0;
}

static int fail_visitor(struct bintree *node, void *arg)
{
    (void)arg;
    (void)node;
    return 1;
}

static int visitor(struct bintree *node, void *arg)
{
    static int idx = 0;
    if (arg == NULL)    /* reset */
        return idx = 0;

    int *arr = (int*)arg;
    arr[idx++] = (int)node->val.sint;

    return 0;
}

static int val_printer(union bintree_data_t val, FILE *out, void *arg)
{
    (void)arg;
    fprintf(out, "%d", (int)val.sint);
    return 0;
}

static void check_dot(struct bintree *root)
{
    FILE *out = fopen("tmp/bintree_dot.dot", "w");
    if (!out)
    {
        int errns = errno;
        fprintf(stderr, "Failed to open file for bintree_dot\n");
        errno = errns;
        perror("fopen()");
        return;
    }

    UT_CHECK(int rc = bintree_fdump_dot(root, val_printer, out, NULL),
             UT_OK(rc == 0)
            );
    fclose(out);
}

static void check_lisp(struct bintree *root)
{
    FILE *out = fopen("tmp/bintree_lisp.txt", "w");
    if (!out)
    {
        int errns = errno;
        fprintf(stderr, "Failed to open file for bintree_lisp\n");
        errno = errns;
        perror("fopen()");
    }

    UT_CHECK(int rc = bintree_fdump_lisp(root, val_printer, out, NULL),
             UT_OK(rc == 0)
            );
    fclose(out);
}

int main()
{
    (void)print_visitor;

    ut_enable_color = 1;
    ut_verbose = 0;
    ut_setup();

    /*
     * A simple manual test
     */
    struct bintree nodes[5];
    for (int i = 0; i < 5; i++)
        nodes[i].val.sint = i;

    /*
     *          0
     *         / \
     *        1   4
     *       / \
     *      2   3
     */
    nodes[0].left  = &nodes[1];
    nodes[1].left  = &nodes[2];
    nodes[1].right = &nodes[3];
    nodes[0].right = &nodes[4];
    nodes[2].left = nodes[2].right = NULL;
    nodes[3].left = nodes[3].right = NULL;
    nodes[4].left = nodes[4].right = NULL;

    /*
     * Check print - can skip... And limiting the scope
     */
    check_dot(&nodes[0]);
    check_lisp(&nodes[0]);

    int walk_result[5];
    int nlr_result[5] = { 0, 1, 2, 3, 4 };
    int lnr_result[5] = { 2, 1, 3, 0, 4 };
    int rnl_result[5] = { 4, 0, 3, 1, 2 };
    int rln_result[5] = { 4, 3, 2, 1, 0 };

    UT_CHECK(int rc = bintree_walk(&nodes[0], "nlr", ok_visitor, NULL),
             UT_OK(rc == 0)
             );
    UT_CHECK(int rc = bintree_walk(&nodes[0], "RnL", fail_visitor, NULL),
             UT_OK(rc == 1)
             );
    UT_CHECK(int rc = bintree_walk(&nodes[0], "nlA", ok_visitor, NULL),
             UT_OK(rc == -1),
             UT_OK(errno == EINVAL)
             );

    bintree_walk(&nodes[0], "nlrn", ok_visitor, NULL);

    /*
     * NLR
     */
    UT_CHECK(int rc = bintree_walk(&nodes[0], "nlr", visitor, walk_result),
             UT_OK(rc == 0)
             );
    for (volatile int i = 0; i < 5; i++)
    {
        UT_CHECK((void)1 /* Placeholder... */,
                 UT_OK(walk_result[i] == nlr_result[i])
                );
    }
    visitor(NULL, NULL);

    /*
     * LNR
     */
    UT_CHECK(int rc = bintree_walk(&nodes[0], "LNR", visitor, walk_result),
             UT_OK(rc == 0)
             );
    for (volatile int i = 0; i < 5; i++)
    {
        UT_CHECK((void)1 /* Placeholder... */,
                 UT_OK(walk_result[i] == lnr_result[i])
                );
    }
    visitor(NULL, NULL);

    /*
     * RNL
     */
    UT_CHECK(int rc = bintree_walk(&nodes[0], "rNl", visitor, walk_result),
             UT_OK(rc == 0)
             );
    for (volatile int i = 0; i < 5; i++)
    {
        UT_CHECK((void)1 /* Placeholder... */,
                 UT_OK(walk_result[i] == rnl_result[i])
                );
    }
    visitor(NULL, NULL);

    /*
     * RLN
     */
    UT_CHECK(int rc = bintree_walk(&nodes[0], "RlN", visitor, walk_result),
             UT_OK(rc == 0)
             );
    for (volatile int i = 0; i < 5; i++)
    {
        UT_CHECK((void)1 /* Placeholder... */,
                 UT_OK(walk_result[i] == rln_result[i])
                );
    }
    visitor(NULL, NULL);

    ut_finalize();

    return EXIT_SUCCESS;
}

