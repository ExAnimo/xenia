
#include "../unittest.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "../vector.h"

int main()
{
    ut_enable_color = 1;
    ut_verbose = 0;
    ut_setup();

    /* Instant fails */
    UT_CHECK(struct vector *v = vector_create(sizeof(int), 0, 0),
             UT_OK(v == NULL)    // 0 capacity
            );
    UT_CHECK(struct vector *v = vector_create(sizeof(int), 10, 20),
             UT_OK(v == NULL)    // length > capacity
            );
    UT_CHECK(struct vector *v = vector_create(0, 20, 10),
             UT_OK(v == NULL)    // elem_size = 0
            );

    /* Normal operations */
    struct vector *v = vector_create(sizeof(int), 8, 0);
    int n_elems = 10;
    for (int i = 0; i < n_elems; i++)
    {
        UT_CHECK(int rc = vector_append(v, &i),
                 UT_OK(rc == 0),
                 UT_OK(vector_length(v) == (size_t)i + 1)
                );
    }

    int more[] = { 1, 2, 3 };
    size_t expected_len = (size_t)n_elems + sizeof(more);
    UT_CHECK(int rc = vector_append_n(v, more, sizeof(more)),
             UT_OK(rc == 0),
             UT_OK(vector_length(v) == expected_len),
             UT_OK(vector_capacity(v) >= vector_length(v)),
             UT_OK(vector_at(v, 0) != NULL),
             UT_OK(vector_at(v, 1) != NULL),
             UT_OK(vector_at(v, 2) != NULL),
             UT_OK(*(int*)vector_at(v, 0) == 0),
             UT_OK(*(int*)vector_at(v, 1) == 1),
             UT_OK(*(int*)vector_at(v, 2) == 2),
             UT_EXECUTE(*(int*)vector_at(v, 1) = 100),
             UT_OK(vector_at(v, 0) != NULL),
             UT_OK(vector_at(v, 1) != NULL),
             UT_OK(vector_at(v, 2) != NULL),
             UT_OK(*(int*)vector_at(v, 0) == 0),
             UT_OK(*(int*)vector_at(v, 1) == 100),
             UT_OK(*(int*)vector_at(v, 2) == 2),
             UT_OK(vector_at(v, expected_len) == NULL),
             UT_OK(vector_at(v, expected_len + 100) == NULL)
            );
    
    UT_CHECK(int *raw = vector_raw_data(v),
             UT_OK(raw != NULL),
             UT_OK(raw[0] == 0),
             UT_OK(raw[1] == 100),
             UT_OK(raw[2] == 2)
            );

    UT_CHECK(void *out = vector_destroy(v, 0),
             UT_OK(out != NULL),
             UT_CLEANUP(free(out))
            );

    v = vector_create(sizeof(int), 8, 0);
    UT_CHECK(void *out = vector_destroy(v, 1),
             UT_OK(out == NULL)
            );

    /* Trying with (char *) */
    const char *strings[] = { "hello", ", ", "Elliot!", "\n" };
    size_t n_strs = sizeof(strings) / sizeof(strings[0]);

    v = vector_create(sizeof(char), 8, 0);
    for (volatile size_t i = 0; i < n_strs; i++)
    {
        const char *str = strings[i];
        UT_CHECK(int rc = vector_append_n(v, str, strlen(str) + 1),
                 UT_OK(rc == 0)
                );
    }

    /* 3rd word's offset */
    size_t offset = 0;
    for (int i = 0; i < 2; i++)
        offset += strlen(strings[i]) + 1;   /* + '\0' */

    UT_CHECK(char *s = vector_at(v, 0),
             UT_OK(s != NULL),
             UT_OK(strcmp(s, strings[0]) == 0),
             UT_EXECUTE(s = vector_at(v, offset)),
             UT_OK(s != NULL),
             UT_OK(strcmp(s, strings[2]) == 0),
             UT_EXECUTE(s = vector_destroy(v, 1)),
             UT_OK(s == NULL)
            );

    ut_finalize();

    return (ut_any_failed ? EXIT_FAILURE : EXIT_SUCCESS);
}

