
#include "../unittest.h"

#include <string.h>
#include <stdlib.h>

#include "../htable.h"

typedef union htable_data_t ht_data;

static size_t hashfunc(ht_data key)
{
    char *str = (char*)(key.vptr);
    size_t hash = 0;
    while (*str)
        hash += *str++;
    return hash;
}
static size_t badhash(ht_data key)
{
    char *a = (char *)(key.vptr);
    return a - a;   // Always 0 - avoiding a warning
}
static int cmp(ht_data a, ht_data b)
{
    return strcmp((char*)a.vptr, (char*)b.vptr);
}

int main()
{
    ut_enable_color = 1;
    ut_verbose = 0;
    ut_setup();

    UT_CHECK(struct htable *ht = htable_create(13, hashfunc, cmp),
             UT_OK(ht != NULL),
             UT_CLEANUP(htable_destroy(ht))
            );

    static char *key[] = { "a", "b", "c", "d",
                           "e", "f", "g", "h",
                           "h", "i", "j", "k",
                           "l", "m", "n", "o",
                           "p", "q", "r", "s",
                           "t", "u", "v", "w",
                           "x", "y", "z", "exists"
                         };
    struct htable *ht = htable_create(13, hashfunc, cmp);

    for (volatile int i = 0; i < (int)(sizeof(key)/sizeof(key[0])); i++)
    {
        struct htable_entry ent = { .key.vptr = (char*)key[i], .val.sint = i };
        UT_CHECK(struct htable_entry *e = htable_add(ht, &ent),
                 UT_OK (e != NULL)
                );
    }

    UT_CHECK(struct htable_entry *e =
                htable_find(ht, (ht_data){.vptr = "does_not_exist"}),
             UT_FAIL(e != NULL)
            );
    UT_CHECK(struct htable_entry *e = htable_find(ht, (ht_data){.vptr = "exists"}),
             UT_OK(e != NULL)
            );

    struct htable_entry  exists = { .key.vptr = "exists", .val.sint = 0 };
    struct htable_entry *pexists = htable_find(ht, (ht_data){.vptr = "exists"});
    UT_CHECK(struct htable_entry *e = htable_add(ht, &exists),
             UT_OK(e == pexists)
            );

    htable_rewind(ht);
    volatile int n = 0;
    while (htable_iterate(ht))
        n++;

    UT_CHECK(int n_entries = n,
             UT_OK(n_entries == (sizeof(key)/sizeof((void*)key[0]) - 1)));

    htable_destroy(ht);

    /* Again, but with a bad hash */
    ht = htable_create(13, badhash, cmp);

    for (volatile int i = 0; i < (int)(sizeof(key)/sizeof(key[0])); i++)
    {
        struct htable_entry ent = { .key.vptr = (char*)key[i], .val.sint = i };
        UT_CHECK(struct htable_entry *e = htable_add(ht, &ent),
                 UT_OK (e != NULL)
                );
    }

    UT_CHECK(struct htable_entry *e =
                htable_find(ht, (ht_data){.vptr = "not_found"}),
             UT_FAIL(e != NULL)
            );
    UT_CHECK(struct htable_entry *e = htable_find(ht, (ht_data){.vptr = "exists"}),
             UT_OK(e != NULL)
            );

    exists = (struct htable_entry){ .key.vptr = "exists", .val.sint = 0 };
    pexists = htable_find(ht, (ht_data){.vptr = "exists"});
    UT_CHECK(struct htable_entry *e = htable_add(ht, &exists),
             UT_OK(e == pexists)
            );

    htable_rewind(ht);
    n = 0;
    while (htable_iterate(ht))
        n++;

    UT_CHECK(int n_entries = n,
             UT_OK(n_entries == (sizeof(key)/sizeof((void*)key[0]) - 1)));

    htable_rewind(ht);
    volatile int val = (int)htable_iterate(ht)->val.sint;
    htable_iterate(ht);
    htable_iterate(ht);
    htable_iterate(ht);
    htable_rewind(ht);

    UT_CHECK(struct htable_entry *e = htable_iterate(ht),
             UT_OK(e->val.sint == val)
            );

    htable_destroy(ht);

    ut_finalize();

    return (ut_any_failed ? EXIT_FAILURE : EXIT_SUCCESS);
}

