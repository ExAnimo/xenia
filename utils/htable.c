
#include <stdlib.h>
#include <assert.h>
#include "htable.h"

/*
 * Hashtable creates a list of entries in case of a collision.
 */
struct htable_entry_list
{
    struct htable_entry         entry;
    struct htable_entry_list   *next;
};

struct htable
{
    size_t                      n_buckets;
    struct htable_entry_list  **buckets;
    hash_func_t                 hash_function;
    cmp_func_t                  cmp_function;

    size_t                      iter_bucket;
    struct htable_entry_list   *iter_entry;
};

struct htable *
     htable_create(size_t n_buckets, hash_func_t hf, cmp_func_t cf)
{
    assert(hf);
    assert(cf);

    struct htable *ht = malloc(sizeof(*ht));
    if (!ht)
        return NULL;
    
    struct htable_entry_list **buckets = malloc(n_buckets * sizeof(*buckets));
    if (!buckets)
    {
        free(ht);
        return NULL;
    }

    /* Could have used calloc() but it is not guaranteed that NULL == 0 */
    for (size_t i = 0; i < n_buckets; i++)
        buckets[i] = NULL;

    ht->n_buckets = n_buckets;
    ht->buckets   = buckets;
    ht->hash_function = hf;
    ht->cmp_function = cf;
    ht->iter_bucket = 0;
    ht->iter_entry = NULL;

    return ht;
}

void htable_destroy(struct htable *ht)
{
    assert(ht);

    for (size_t i = 0; i < ht->n_buckets; i++)
    {
        struct htable_entry_list *curr = ht->buckets[i];
        while (curr)
        {
            struct htable_entry_list *next = curr->next;
            free(curr);
            curr = next;
        }
    }
    free(ht->buckets);
    free(ht);
}

struct htable_entry *
     htable_add(struct htable *ht, struct htable_entry *e)
{
    assert(ht);
    assert(e);

    struct htable_entry *exist = htable_find(ht, e->key);
    if (exist)
        return exist;

    /* Key not found - create a new entry */
    struct htable_entry_list *l = malloc(sizeof(*l));
    if (!l)
        return NULL;

    size_t hash = ht->hash_function(e->key) % ht->n_buckets;
    l->entry = *e;
    l->next  = ht->buckets[hash];
    ht->buckets[hash] = l;

    return &l->entry;
}

struct htable_entry *
     htable_find(struct htable *ht, union htable_data_t key)
{
    assert(ht);

    size_t hash = ht->hash_function(key) % ht->n_buckets;
    struct htable_entry_list *l = ht->buckets[hash];
    while (l)
        if (ht->cmp_function(l->entry.key, key) == 0)
            return &l->entry;
        else
            l = l->next;

    return NULL;
}

struct htable_entry *
     htable_iterate(struct htable *ht)
{
    assert(ht);

    if (ht->iter_entry)
    {
        struct htable_entry *e = &ht->iter_entry->entry;
        ht->iter_entry = ht->iter_entry->next;
        return e;
    }

    /* Looking for the next non-empty bucket */
    for (size_t i = ht->iter_bucket + 1; i < ht->n_buckets; i++)
        if (ht->buckets[i])
        {
            struct htable_entry *e = &ht->buckets[i]->entry;
            ht->iter_bucket = i;
            ht->iter_entry = ht->buckets[i]->next;
            return e;
        }

    /* The buckets ended */
    return NULL;
}

void htable_rewind(struct htable *ht)
{
    assert(ht);

    ht->iter_bucket = 0;
    ht->iter_entry = ht->buckets[0];
}

