
/*
 * A simple single-header logging utility.
 *
 * Lacks many features of an "industrial-strenght" loggers but it is
 * primatily used internally for debugging and can be substituted
 * later if needed.
 *
 * The main use is to trace the successful path - the errors are
 * expected to be printed anyways by the 'clean' code.
 */

#include <stdio.h>

#ifndef   NDEBUG
#  define LOG(...)                                                      \
    do                                                                  \
    {                                                                   \
        fprintf(stderr, "%s: %d: %s(): ", __FILE__, __LINE__, __func__);\
        fprintf(stderr, __VA_ARGS__);                                   \
    }                                                                   \
    while (0)

/*
 * Be carefull to not to call any functions with side effects!
 */
#  define LOG_CALL(expr)                                                \
    do                                                                  \
    {                                                                   \
        LOG("\n");                                                      \
        expr;                                                           \
    }                                                                   \
    while (0)

#else
#  define LOG(...)
#  define LOG_CALL(expr)
#endif // NDEBUG

