
/*
 * String arena API.
 *
 * An implementation of arena allocator for strings, inspired by
 * David. R. Hanson "C Interfaces and Implementations". The data structure
 * here is a mix of arena and atom, but:
 *  1. it does not raise exceptions and follows the normal conventions
 *     in this project;
 *  2. unlike atom, the strings can be freed.
 *
 * The need for this data structure arose with the introduction of a big
 * amount of strings used as keys and values for the htable. These strings
 * could either be allocated dynamically or stored in a common array. The
 * first approach requires many calls to malloc() and careful free() matching,
 * the second one was adopted at first, but proved to be troublesome:
 * realloc() can move the array, so the addresses in the htable must be
 * corrected. Adding a constant offset is a viable solution, but it is a hack
 * since it does not conform to the standard. Use of offsets and converting
 * them back to pointers on finish is cumbersome. So the string arena
 * solves the two problems:
 *  1. it never reallocates returned strings;
 *  2. all the strings can be deallocated with one call.
 *
 * Usage example (no checks for simplicity):
 *
 * struct str_arena *arena = str_arena_create(0); // use default buffer size
 * 
 * char buf[256];
 * for (int i = 0; i < 10; i++)
 * {
 *     fgets(buf, sizeof(buf), stdin);
 *     char *s = str_arena_store(arena, buf);
 *
 *     struct htable_entry ent = {};
 *     ent.key.vptr = s;
 *     ent.val.sint = i;
 *     htable_add(ht, &ent);
 * }
 *
 * // ht can be used until the call to free:
 * str_arena_destroy(arena);
 */

#ifndef   STR_ARENA_H_INCLUDED
#define   STR_ARENA_H_INCLUDED

#include <stddef.h>

/*
 * String arena handler.
 */
struct str_arena;

/*
 * Creates a new string arena with size, approximately equal to 'size_hint':
 * subsequent necessary allocations will be of that size.
 *
 * Returns NULL in case of failure and sets errno appropriately, a valid
 * pointer otherwise.
 */
struct str_arena *
     str_arena_create(size_t size_hint);

/*
 * Copies the null-terminated 'str' to 'arena'. 'str' is stored with the
 * terminating '\0'.
 *
 * Returns NULL in case of failure and sets errno appropriately, a pointer
 * to the stored string otherwise. The returned value can safely be used
 * as a key or a value for htable.
 */
char *
     str_arena_store(struct str_arena *arena, const char *str);

/*
 * Deallocates arena.
 *
 * All the pointers to strings are invalid after the call to
 * str_arena_destroy().
 */
void str_arena_destroy(struct str_arena *arena);

#endif // STR_ARENA_H_INCLUDED

