
/*
 * Hash table API.
 *
 * Usage example (no checks for simplicity):
 *
 *  // 1. Create and fill ht with values
 *
 *  struct htable *ht = htable_create(13, str_hash, strcmp);
 *  struct htable_entry ent = {};
 *  char key[] = "a";
 *  for (int i = 0; i < 20; i++)
 *  {
 *      ent.key.vptr = a;
 *      ent.val.sint = i;
 *      htable_add(ht, &ent);
 *
 *      key[0]++;
 *  }
 *
 *  // 2. Query ht
 *
 *  struct htable_entry *e;
 *  e = htable_find(ht, (union data_t){.vptr = "does_not_exist"});
 *  assert(e == NULL);
 *  e = htable_find(ht, (union data_t){.vptr = "a"});
 *  assert(e !- NULL);
 *
 *  // 3. Iterate over the htable:
 *
 *  htable_rewind(ht);  // Always call after add
 *  while ((e = htable_iterate(ht)))
 *      printf("found key '%s', val = '%d'\n", (char*)e->key.vptr, e->val.sint);
 *
 *  // 4. Delete ht
 *
 *  htable_destroy(ht);
 *  
 * Note that htable_destroy() only deletes memory occupied by a hashtable
 * and does not touch 'key' and 'val'. In case they were dynamically
 * allocated, the caller should free them.
 *
 * Another important aspect is that htable_add() merely copies the given
 * entry. Thus 'key' field should not point to a buffer you are going to
 * rewrite.
 *
 * TIPS:
 *  1. You may find it useful to typedef htable_* types. No typedefs are
 *     provided to not to hide the structure of types.
 *  2. Suggested typedefs:
 *      typedef union htable_data_t ht_data;
 *      typedef struct htable_entry ht_pair;
 */

#ifndef   HTABLE_H_INCLUDED
#define   HTABLE_H_INCLUDED

#include <stddef.h>
#include <stdint.h>

/*
 * Hashtable handle.
 */
struct htable;

/*
 * Data-holding union.
 *
 * Use 'vptr' to store arbitrary data in some extern storage.
 * Signed (s-) and unsigned (u-) fields are only supported for
 * convenience for common case to store integers.
 */
union htable_data_t
{
    intmax_t    sint;
    uintmax_t   uint;
    void       *vptr;
};

/*
 * The htable entry. One can see it as a pair <key, val>.
 */
struct htable_entry
{
    union htable_data_t key;
    union htable_data_t val;
};

typedef size_t (*hash_func_t)(union htable_data_t key);
typedef int (*cmp_func_t)(union htable_data_t key1, union htable_data_t key2);

/*
 * Creates a new hashtable with 'n_buckets' buckets, 'hf' hash function and
 * 'cf' comparison function.
 *
 * 'cf' is expected to return 0 if the keys are identical, !0 otherwise.
 *
 * Returns NULL in case of failure and sets errno appropriately, a valid
 * pointer otherwise.
 */
struct htable *
     htable_create(size_t n_buckets, hash_func_t hf, cmp_func_t cf);

/*
 * Deallocates inner resources, does not delete htable_entry's field
 * pointers.
 */
void htable_destroy(struct htable *ht);

/*
 * Adds an entry 'e' to a 'ht'. Both 'ht' and 'e' should not be NULL.
 *
 * Returns pointer to the inserted entry or to an existing one if the key
 * already existed. Returns NULL in case of failure and sets errno
 * appropriately.
 */
struct htable_entry *
     htable_add(struct htable *ht, struct htable_entry *e);

/*
 * Returns an entry corresponding to a given 'key'. If not found, returns
 * NULL.
 */
struct htable_entry *
     htable_find(struct htable *ht, union htable_data_t key);

/*
 * Iterates over all the keys: each subsequent call returns a pointer to
 * the next entry. The order is random. When all the entries were visited,
 * retrns NULL.
 *
 * To start from the beginning call htable_rewind().
 *
 * After inserting new elements call htable_rewind().
 */
struct htable_entry *
     htable_iterate(struct htable *ht);

/*
 * Restores initial state of the htable: next call to htable_iterate() will
 * return the first entry.
 */
void htable_rewind(struct htable *ht);

#endif // HTABLE_H_INCLUDED

