
/*
 * Vector API.
 *
 * A vector contains values of arbitrary type inside. Rougthly mirrors
 * the interface of std::vector.
 *
 * Usage example (checks omitted for simplicity):
 *
 * // 1. Create a vector
 * 
 * struct vector *v = vector_create(sizeof(int), 8, 0);
 *
 * // 2. Fill it with values
 *
 * for (int i = 0; i < 10; i++)
 *     vector_append(v, &i);
 *
 * int more_elems[] = { 1, 2, 3 };
 * vector_append_n(v, more_elems, sizeof(more_elems));
 *
 * // 3. Query for data
 *
 * printf("3'rd elem is %d\n", *(int*)vector_at(v, 3));
 *
 * // 4. Delete the array but retain all the data:
 *
 * size_t n_elems = vector_length(v);
 * const int *data = vector_destroy(v, 0);
 *
 * TIPS:
 *  1. Probably it would be useful to define wrappers for concrete types.
 *      For example:
 *
 *      int vector_get_unsafe(struct vector *v, size_t idx) {
 *          return *vector_at(v, idx);
 *      }
 *      struct point *vector_steal(struct vector *v) {
 *          struct point *p = vector_destroy(v, 0);
 *          return p;
 *      }
 *
 *  2. The interface is particulary useful to store strings in the form
 *      ['s', 't', 'r', '1', '\0', 's', '2', '\0']. The vector can be
 *      queried then like this:
 *
 *      char *str = vector_at(v, str_offset);
 */

#ifndef   VECTOR_H_INCLUDED
#define   VECTOR_H_INCLUDED

#include <stddef.h>

/*
 * The vector handle. Can be changed later to enable inlining
 * of some API functions.
 *
 * The handle must be a pointer since it is to be passed to functions
 * that must change its state.
 *
 * POSIX reserves _t suffix for future use, so it was decided to
 * not to typedef the structure.
 */
struct vector;

/*
 * Creates a new vector with initial capacity set to 'capacity', initial
 * length set to 'length' and element size 'element_size'.
 *
 * 'capacity' must be >= 'length', 'elem_size' > 0 and 'capacity' > 0.
 *
 * Returns NULL in case of failure and sets errno appropriately, a valid
 * pointer otherwise.
 */
struct vector *
    vector_create(size_t elem_size, size_t capacity, size_t length);

/*
 * Resizes the vector 'v' so that it has capacity 'new_cap'. The behaviour
 * is identical to that of realloc(). If new capacity is less than length,
 * the length is set equal to the new capacity.
 *
 * 'new_cap' indicates new capacity in units of elements (not bytes!) and
 * MUST be > 0.
 *
 * If memory allocation fails, the vector remains in a valid state that it
 * had before the call.
 *
 * Returns (-1) in case of error and sets errno appropriately, 0 otherwise.
 */
int vector_resize(struct vector *v, size_t new_cap);

/*
 * Resizes, if necessary, the vector 'v' (new cap is twice as big as old)
 * and appends 'new_elem' to it.
 *
 * Returns (-1) in case of error and sets errno appropriately, 0 otherwise.
 */
int vector_append(struct vector *v, const void *new_elem);

/*
 * Appends 'n_elems' element pointed to by 'new_elems' to the vector 'v'.
 * Resizes vector so that it is big enough to store all the elements.
 *
 * Returns (-1) in case of error and sets errno appropriately, 0 otherwise.
 */
int vector_append_n(struct vector *v, const void *new_elems, size_t n_elems);

/*
 * Returns 'idx''th element of the vector 'v'. If 'idx' >= vector's
 * length, NULL is returned.
 */
void *vector_at(struct vector *v, size_t idx);

/*
 * Returns the capacity of the vector 'v'.
 */
size_t vector_capacity(struct vector *v);

/*
 * Returns the length of the vector 'v'.
 */
size_t vector_length(struct vector *v);

/*
 * Returns the raw data array of the vector 'v'.
 *
 * It can be safely casted to the type of elements in it if the size of
 * the element size, supplied at the initialization time, was obtained
 * via sizeof.
 *
 * The returned array remains valid until resize or append operations
 * were performed on the array.
 */
void *vector_raw_data(struct vector *v);

/*
 * Deallocates vector resources. It is not an error to pass NULL pointer
 * as 'v'.
 *
 * If 'with_data' != 0, the underlying array is deleted as well and NULL is
 * returned. Otherwise this array is returned and the caller is responsible
 * for freeing it.
 */
void *vector_destroy(struct vector *v, int with_data);

#endif // VECTOR_H_INCLUDED

