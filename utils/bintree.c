
#include "bintree.h"

#include <stdlib.h>
#include <assert.h>
#include <errno.h>

/*
 * TODO: check whether it will improve performance to check for child != NULL
 * before the recursive call.
 */
int bintree_walk(struct bintree *root, const char *order,
                 traverse_func_t tf, void *arg)
{
    assert(order);
    assert(tf);

    if (!root)
        return 0;

    const char *save_order = order;
    int step;
    while ((step = *order++))
    {
        int rc;
        switch (step)
        {
            case 'l':
            case 'L':
                rc = bintree_walk(root->left,  save_order, tf, arg); break;
            case 'r':
            case 'R':
                rc = bintree_walk(root->right, save_order, tf, arg); break;
            case 'n':
            case 'N':
                rc = tf(root, arg);                                  break;
            default:
                errno = EINVAL;
                return -1;
        }
        if (rc != 0)
            return (rc < 0 ? -1 : 1);
    }

    return 0;
}

// Dump ============================================================
/*
 * New dump formats should stick to this format:
 *  1. bintree_{f|s}dump_fmt() - {file|string} output function, that
 *      prints header and footer to the output stream and calls
 *      bintree_walk() to print each node.
 *  2. bintree_{f|s}dump_fmt_traverse() - a wrapper for the printing func,
 *      that effectively converts it to traverse_func_t.
 *  3. struct {f|s}dump_bind - a struct to bind output args to the
 *      printing func.
 *
 * OR
 *
 * Sometimes direct tree traversal is much simpler than using the walk
 * function. It could have accomodate more possible cases, but its
 * signature and usage would become too complex. For such cases, try
 * implementing the three walk by yourself.
 *
 * Use functions in this file as a reference implementation: feel free
 * to start with copy and paste :)
 */

/*
 * A binding struct to allow for "cast" from value_func_t to the
 * traverse_func_t.
 */
struct fdump_bind
{
    fvalue_func_t vf;
    FILE *outfile;
    void *arg;
};

// DOT  ============================================================

/*
 * A traverse wrapper for printing function. Prints a single node.
 */
static int bintree_fdump_dot_traverse(struct bintree *root, void *arg)
{
    assert(arg);

    if (!root)
        return 0;

    struct fdump_bind *bind = arg;
    FILE *out = bind->outfile;

    /* 1. Print node itself */
    fprintf(out, "\tn_%p [label=\"{ %p | ", (void*)root, (void*)root);
    int ret = bind->vf(root->val, bind->outfile, bind->arg);
    fprintf(out, " | { <lc> left\\n%p | <rc> right\\n%p }}\"]\n",
            (void*)&root->left, (void*)&root->right);

    /* 2. Print node connections */
    if (root->left)
        fprintf(out, "\tn_%p:lc -> n_%p\n", (void*)root, (void*)root->left);
    if (root->right)
        fprintf(out, "\tn_%p:rc -> n_%p\n", (void*)root, (void*)root->right);

    /* 3. Returns the node print func value */
    return ret;
}

/*
 * Prints a header and a footer to the file and calls recursively
 * printing for each node.
 *
 * TODO: does not check any IO errors.
 */
int bintree_fdump_dot(struct bintree *root, fvalue_func_t vf,
                      FILE *outfile, void *arg)
{
    assert(outfile);
    assert(vf);

    if (!root)
        return 0;

    /*
     * Tabulations and newlines are inserted to facilitate debugging
     * and manual interactions.
     *
     * Header
     */
    fprintf(outfile, "digraph {\n");
    fprintf(outfile, "\tnode[shape=\"record\"]\n");
    fprintf(outfile, "\tedge[]\n\n");

    /* Node printing */
    struct fdump_bind bind = { .vf = vf, .outfile = outfile, .arg = arg };
    int ret = bintree_walk(root, "nlr", bintree_fdump_dot_traverse, &bind);

    /*
     * Footer
     *
     * Try to finish even if node printing function failed.
     */
    fprintf(outfile, "}\n");

    return (ret == 0) ? 0 : 1;
}


// Lisp ============================================================

/* Reimplements tree walk. */
int bintree_fdump_lisp(struct bintree *root, fvalue_func_t vf,
                       FILE *outfile, void *arg)
{
    assert(outfile);
    assert(vf);

    if (!root)
        return 0;

    int ret;

    /* No header needed */

    fprintf(outfile, " ( ");
    ret = vf(root->val, outfile, arg);
    if (ret != 0)
        return 1;

    ret = bintree_fdump_lisp(root->left,  vf, outfile, arg);
    if (ret != 0)
        return 1;

    ret = bintree_fdump_lisp(root->right, vf, outfile, arg);
    if (ret != 0)
        return 1;

    fprintf(outfile, " )");

    /* No footer needed */

    return 0;
}

