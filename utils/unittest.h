
/*
 * Unittest framework. Designed to be located inside a single .h file.
 * MUST BE THE FIRST FILE INCLUDED.
 *
 * To use the file, simply include it. Before you can call tests,
 * call ut_setup() macro and after the tests are done, call ut_finalize().
 *
 * The tests are designed to handle many different cases and not only
 * check the return value of the function. Moreover, you can test almost
 * any C statement. To construct a test, follow the next example:
 *
 * UT_CHECK(int ret = summator(3, 4),
 *          UT_OK  (ret == 7),
 *          UT_OK  (ret != 34),
 *          UT_FAIL(ret == 0)
 *         )
 * UT_CHECK(side_effect(&big_struct),
 *          UT_OK  (big_struct == correct_output)
 *         )
 *
 * The tested statement goes as the first argument, the effects of the
 * call are tested using UT_OK and UT_FAIL for expected correct and
 * incorrect statements, respectively. Note that all the tests are
 * undertaken in the declaration order. If one fails, subsequent cases
 * will be run except for the signal. If any test in UT_CHECK fails,
 * ut_test_failed is set to 1, otherwise it is 0. If any test at all
 * fails, ut_any_failed is set to 1.
 *
 * Additional options exist: UT_EXECUTE() and UT_CLEANUP(). The code
 * given as argument to UT_EXECUTE() is executed in order like in the
 * following example:
 *
 * UT_CHECK(int x = 5,
 *          UT_OK(x == 5),
 *          UT_EXECUTE(x = 42),
 *          UT_OK(x == 42)
 *         );
 * // All tests passed
 *
 * UT_CLEANUP() is similar to UT_EXECUTE() but the given code will be
 * executed after all the tests are run.
 *
 * The code in this header uses setjmp()/longjmp() functions so in order
 * to avoid compiler warnings use can declare the variables not used 
 * directly in tests (such as loop counters) as volatile.
 *
 * By default unittest.h prints only failed tests as
 * 'The practice of programming' by Kernighan and Pike advises. Though,
 * this behaviour can be altered by setting optional flags (set them
 * before ut_setup()):
 *
 *  - ut_verbose        - print successful tests as well;
 *  - ut_enable_color   - print coloured output (false when stdout was
 *                        redirected to a file)
 * POSIX-only... GNU as well
 */

#ifndef   UNITTEST_H_INCLUDED
#define   UNITTEST_H_INCLUDED

#define _GNU_SOURCE

#include <unistd.h>
#include <setjmp.h>
#include <signal.h>
#include <stdio.h>

static sigjmp_buf               ut_jmpbuf;
static jmp_buf                  ut_cleanup_buf;
static jmp_buf                  ut_testout_buf;
static int                      ut_cleanup_set;
static volatile sig_atomic_t    ut_canjmp;
static volatile sig_atomic_t    ut_sig;
static struct sigaction         ut_new_act, ut_old_act;
static int                      ut_status;  // errno-like :)
static int                      ut_dummy;

static char                    *ut_test_function;
static int                      ut_test_num;

static int                      ut_enable_color;
static int                      ut_verbose;
static int                      ut_color_supported;
static int                      ut_test_failed;
static int                      ut_any_failed;

/*
 * Catches a signal, saves its value and jumps to the 'safe' place when
 * the signal was expected. If the jmpbuf was not set, just returns.
 */
static void ut_sighandler(int signum)
{
    if (ut_canjmp == 0)
        return;
    ut_sig = signum;
    ut_canjmp = 0;
    siglongjmp(ut_jmpbuf, 1);
}

/*
 * Returns (-1) in case of failure and sets errno appropriately, 0 otherwise.
 */
static int ut_set_sighandler()
{
    ut_new_act.sa_handler = ut_sighandler;
    // Do not restore previous handler after receiving a signal:
    ut_new_act.sa_flags = 0;
    sigemptyset(&ut_new_act.sa_mask);
    sigaddset(&ut_new_act.sa_mask, SIGSEGV);
    sigaddset(&ut_new_act.sa_mask, SIGABRT);
    sigaddset(&ut_new_act.sa_mask, SIGFPE);
    /*
     * Safe to set signals in a row without restoring disposition
     * in case of failure: ut_canjmp == 0 so the handler will not use
     * invalid jmpbuf.
     */
    if (sigaction(SIGSEGV, &ut_new_act, &ut_old_act))
    {
        perror("sigaction(SIGSEGV)"); 
        return -1;
    }
    if (sigaction(SIGABRT, &ut_new_act, NULL))
    {
        perror("sigaction(SIGABRT)");
        return -1;
    }
    if (sigaction(SIGFPE,  &ut_new_act, NULL))
    {
        perror("sigaction(SIGFPE)");
        return -1;
    }
    return 0;
}

/*
 * Unblocks all the signals blocked by ut_set_sighandler().
 *
 * Actually, the function does its best to succeed. In case any signal
 * failed to be restored, returns 0. No guaranties regarding errno...
 */
static int ut_ret_sighandler()
{
    int ret = 0;
    if (sigaction(SIGSEGV, &ut_old_act, NULL))
        ret = -1;
    if (sigaction(SIGABRT, &ut_old_act, NULL))
        ret = -1;
    if (sigaction(SIGFPE,  &ut_old_act, NULL))
        ret = -1;
    return ret;
}

/*
 * Performs necessary preparations for testing utilities.
 *
 * Must be a macro since it sets jumpbuf - we cannot return from a
 * function that did it.
 */
#define ut_setup()                                              \
    do                                                          \
    {                                                           \
        /* Just init, do not set canjmp */                      \
        sigsetjmp(ut_jmpbuf, 1);                                \
        ut_status = ut_set_sighandler();                        \
        ut_test_num = 0;                                        \
        ut_test_function = NULL;                                \
        ut_color_supported = isatty(STDOUT_FILENO);             \
        ut_enable_color = ut_enable_color && ut_color_supported;\
        ut_test_failed = 0;                                     \
        ut_any_failed = 0;                                      \
        ut_cleanup_set = 0;                                     \
    } while (0)

/*
 * Cleanup function.
 *
 * Ignores possible failures - TODO?
 */
#define ut_finalize()                                           \
    do                                                          \
    {                                                           \
        ut_status = ut_ret_sighandler();                        \
        ut_canjmp = 0;                                          \
        ut_test_function = NULL;                                \
    } while (0)                                                 \

#define UT_RED(str)     "\x1b[31m" str "\x1b[0m"
#define UT_GREEN(str)   "\x1b[32m" str "\x1b[0m"

static void ut_print_test_result(int test_ok, const char *condstr,
                                 const char * expected, int signal)
{
    printf("[%4d]", ut_test_num);
    if (ut_enable_color)
        printf("[%s] ", (test_ok? UT_GREEN(" OK ") : UT_RED("FAIL")));
    else
        printf("[%s] ", (test_ok? " OK " : "FAIL"));
    printf("%s:\n    ", (ut_test_function ? ut_test_function : "(?)"));
    printf("Expected %s: %s", expected, condstr);
    if (signal)
        printf(", signal: %d", signal);
    printf("\n");
}

// Main macros =====================================================

/*
 * Using dummies to effectively remove commas in __VA_ARGS__ expansion
 */
#define UT_TEST(cond, expected, condstr)                        \
    ut_dummy += 1;                                              \
    if ((cond))                                                 \
    {                                                           \
        ut_print_test_result(0, (condstr), (expected), 0);      \
        ut_test_failed = 1;                                     \
        ut_any_failed = 1;                                      \
    }                                                           \
    else if (ut_verbose)                                        \
    {                                                           \
        ut_print_test_result(1, (condstr), (expected), 0);      \
    }                                                           \
    ut_dummy += 1

#define UT_OK(cond)     UT_TEST(!(cond), " ok ", #cond)
#define UT_FAIL(cond)   UT_TEST((cond),  "fail", #cond)

#define UT_CLEANUP(code)                                        \
    ut_dummy += 1;                                              \
    ut_cleanup_set = 1;                                         \
    if (setjmp(ut_cleanup_buf))                                 \
    {                                                           \
        code ;                                                  \
        longjmp(ut_testout_buf, 1);                             \
    }                                                           \
    ut_dummy += 1

#define UT_EXECUTE(code)                                        \
    ut_dummy += 1;                                              \
    {                                                           \
        code ;                                                  \
    }                                                           \
    ut_dummy += 1


#define UT_CHECK(checkstmt, ...)                                \
    do                                                          \
    {                                                           \
        ut_test_num++;                                          \
        ut_test_function = #checkstmt ;                         \
        ut_canjmp = 1;                                          \
        ut_test_failed = 0;                                     \
        ut_cleanup_set = 0;                                     \
        /* Returned from handler? */                            \
        if (sigsetjmp(ut_jmpbuf, 1))                            \
        {                                                       \
            ut_print_test_result(0, #checkstmt, " ok ", ut_sig);\
            ut_test_failed = 1;                                 \
            ut_any_failed = 1;                                  \
        }                                                       \
        else                                                    \
        {                                                       \
            checkstmt ;                                         \
            __VA_ARGS__ ;                                       \
        }                                                       \
        /* Cleanup */                                           \
        if (ut_cleanup_set && setjmp(ut_testout_buf) == 0)      \
            longjmp(ut_cleanup_buf, 1);                         \
    } while (0)

#endif // UNITTEST_H_INCLUDED

