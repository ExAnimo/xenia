
/*
 * TODO: implement non-recursive version to traverse deep trees.
 * A stack will be needed: try using vector. Or build a real stack
 * on top of vector.
 */

/*
 * Binary tree API.
 *
 * Implemets the similar API as the htable.
 *
 * The main difference is that the tree node structure should be known to the
 * user of the tree. htable has to act as a key-value store and should not
 * export its internals, while the bintree is a well-defined structure of
 * { left, right, value }.
 *
 * Usage example (no checks for simplicity):
 *
 *  // 1. Simple visitor-enumerator
 *
 *  static int visitor(struct bintree *node, void *arg)
 *  {
 *      static int idx = 0;
 *  
 *      int *arr = (int*)arg;
 *      arr[idx++] = (int)node->val.sint;
 *  
 *      return 0;
 *  }
 *
 *  // 2. Set up a tree
 *
 *  struct bintree nodes[5];
 *  for (int i = 0; i < 5; i++)
 *      nodes[i].val.sint = i;
 *
 *  //
 *  //         0
 *  //        / \
 *  //       1   4
 *  //      / \
 *  //     2   3
 *  //
 *
 *  nodes[0].left  = &nodes[1];
 *  nodes[1].left  = &nodes[2];
 *  nodes[1].right = &nodes[3];
 *  nodes[0].right = &nodes[4];
 *  nodes[2].left = nodes[2].right = NULL;
 *  nodes[3].left = nodes[3].right = NULL;
 *  nodes[4].left = nodes[4].right = NULL;
 *
 * // 3. Run the visitor
 *
 *  int walk_result[5];
 *  int nlr_result[5] = { 0, 1, 2, 3, 4 };
 *
 *  bintree_walk(&nodes[0], "nlr", visitor, walk_result)
 *
 *  for (int i = 0; i < 5; i++)
 *      assert((int)nodes[i].val.sint == walk_result[i]);
 *
 */

#ifndef   BTREE_H_INCLUDED
#define   BTREE_H_INCLUDED

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

/*
 * Data-holding union.
 *
 * Use 'vptr' to store arbitrary data in some extern storage.
 * Signed (s-) and unsigned (u-) fields are only supported for
 * convenience for common case to store integers.
 */
union bintree_data_t
{
    intmax_t    sint;
    uintmax_t   uint;
    void       *vptr;
};

/*
 * Binary tree handle.
 *
 * Full definition is given in a header to enable simple access to the
 * child pointers or create an array of nodes ahead.
 */
struct bintree
{
    struct bintree      *left;
    struct bintree      *right;

    union bintree_data_t val;
};

typedef int (*traverse_func_t)(struct bintree *node, void *supplementary_args);

/*
 * Interface inspired by "The Practice of Programming" by Kernighan and Pike.
 * The function defines its own little "programming language" to define the
 * walk order.
 *
 * Walks the tree from the 'root' and apply 'tf' to each node. 'tf' must take
 * a pointer to the node and an additional argument that is forwarded by the
 * traverse function to 'tf' on each call. This argument is passed as 'arg'.
 *
 * 'tf' will not be called with NULL.
 *
 * The walk order is specified as a string containing characters 'l', 'r', 'n'
 * that stand for "left child", "right child", "node". There is no
 * restriction on the string's length.
 *
 * 'tf' must return non-negative values. The function stops walking if 'tf'
 * returns non-zero value.
 *
 * Returns 1 in case 'tf' returned non-zero, 0 in case everything is OK,
 * (-1) in case of error and sets errno to
 *  EINVAL - 'order' contains illegal characters
 *
 * WARNING: calls itself recursively: be sure not to process too deep trees.
 */
int bintree_walk(struct bintree *root, const char *order,
                 traverse_func_t tf, void *arg);


typedef int (*fvalue_func_t)(union bintree_data_t val, FILE* outfile,
                             void *supplementary_args);

/*
 * Dumps tree 'root' to the file 'outfile' as a DOT graph for Graphviz.
 *
 * 'vf' is used to print the 'val' field in each node. The function is
 * expexted not to change the value of the 'val', so it is passed by
 * value. The values should be printed as strings or numbers and delimeted
 * with pipe symbol ('|'). If a function fails, it should return non-zero
 * value and 0 otherwise.
 *
 * Returns 1 in case 'tf' returned non-zero, 0 in case everything is OK,
 * (-1) in case of error.
 */
int bintree_fdump_dot(struct bintree *root, fvalue_func_t vf,
                      FILE *outfile, void *arg);

/*
 * Dumps tree 'root' to the file 'outfile' in a Lisp notation.
 *
 * 'vf' is used to print the 'val' field in each node. The function is
 * expexted not to change the value of the 'val', so it is passed by
 * value. If a function fails, it should return non-zero
 * value and 0 otherwise.
 *
 * Returns 1 in case 'tf' returned non-zero, 0 in case everything is OK,
 * (-1) in case of error.
 */
int bintree_fdump_lisp(struct bintree *root, fvalue_func_t vf,
                       FILE *outfile, void *arg);

#endif // BTREE_H_INCLUDED

