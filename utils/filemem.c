
#define _GNU_SOURCE

#include <assert.h>
#include <stdint.h>
#include <errno.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#include "filemem.h"

/*
 * POSIX version.
 */

/*
 * utils.c keeps track of service info in a circular buffer. Memory for the
 * buffer is allocated with mmap(). unmap() is not called: the memory is
 * automatically unmapped after the process terminates.
 */
typedef uint8_t list_size_t;
const size_t RECORDS_MAX = (list_size_t)(-1) + 1;
struct utils_file_record
{
    void   *addr;
    size_t  length;
};
struct utils_file_record_hdr
{
    struct utils_file_record   *base;
    list_size_t                 head;
    list_size_t                 tail;
};

static int UTILS_INITED;
static struct utils_file_record_hdr RECORDS;

static size_t PAGESIZE;
#define ALIGN_TO_PAGE(num) (((num) + PAGESIZE - 1) / PAGESIZE)

/*
 * Allocates next free record.
 *
 * Returns a pointer to allocated record or NULL if no space left.
 */
static struct utils_file_record *get_free_record()
{
    assert(UTILS_INITED);

    if ((list_size_t)(RECORDS.head + 1) == RECORDS.tail)
        return NULL;
    return RECORDS.base + RECORDS.head++;
}

/*
 * Finds the record by its 'addr' field value.
 *
 * Returns a pointer to the requested record or NULL if it was not found.
 */
static struct utils_file_record *find_record(const void *addr)
{
    assert(UTILS_INITED);

    for (list_size_t i = RECORDS.tail; i != RECORDS.head; i++)
        if ((RECORDS.base + i)->addr == addr)
            return RECORDS.base + i;
    return NULL;
}

/*
 * Deletes the record 'rec'.
 */
static void delete_record(struct utils_file_record *rec)
{
    assert(UTILS_INITED);
    assert(rec);
    assert(rec >= RECORDS.base);
    assert(rec <  RECORDS.base + RECORDS_MAX);

    // Just rewriting the record with the tail
    *rec = *(RECORDS.base + RECORDS.tail++);
}

/*
 * Inits the lib if necessary. Returns 0 if all is OK, (-1) otherwise and sets
 * errno appropriately.
 */
static int init_utils()
{
    if (UTILS_INITED)
        return 0;

    // TODO: use sysconf to obtain the value dynamically
    PAGESIZE = 0x1000;
    size_t records_len = sizeof(*RECORDS.base) * sizeof(RECORDS.head);
    records_len = ALIGN_TO_PAGE(records_len);

    RECORDS.base = mmap(NULL, records_len, PROT_READ | PROT_WRITE,
                   MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (RECORDS.base == MAP_FAILED)
        return -1;

    RECORDS.head =  0;
    RECORDS.tail = -1;
    (RECORDS.base + RECORDS.tail)->addr = NULL;
    (RECORDS.base + RECORDS.tail)->length = -1;
    UTILS_INITED = 1;

    return 0;
}

// API functions ===================================================

void *utils_file_to_memory(const char *filename, const char *mode,
                           size_t *filelen)
{
    assert(filename);
    assert(mode);

    int errns = 0;

    init_utils();

    // Immediate check for free space
    struct utils_file_record *rec = get_free_record();
    if (!rec)
    {
        errno = EBADSLT;
        return NULL;
    }

    // Getting the mode
    int prot = 0;
    int flag = O_RDONLY;    // Necessary for mapping so by default
    const char *c = mode;
    while (*c)
        switch (*c++)
        {
            case 'r':
            case 'R':
                prot |= PROT_READ;
                // flag is already set
                break;
            case 'w':
            case 'W':
                prot |= PROT_WRITE;
                flag  = O_RDWR;
                break;
            default:
                errno = EBADRQC;
                goto err_quit_0;
        }

    // Opening, getting file's size, mapping
    int fd = open(filename, flag);
    if (fd == -1)
        goto err_quit_0;

    struct stat st;
    int rc = fstat(fd, &st);
    if (rc < 0)
    {
        errns = errno;
        goto err_quit_1;
    }

    size_t file_len = st.st_size;
    if (filelen)
        *filelen = file_len;
    if ((prot & PROT_READ) && file_len == 0)
    {
        errns = EBADFD;
        goto err_quit_1;
    }

    // +1 for '\0'. mmap() fills with 0 byte automatically.
    void *mapped = mmap(NULL, file_len + 1, prot, MAP_SHARED, fd, 0);
    if (mapped == MAP_FAILED)
    {
        errns = errno;
        goto err_quit_1;
    }

    // Finilizing
    rec->addr = mapped;
    rec->length = file_len;
    close(fd);
    return mapped;

err_quit_1:
    close(fd);
    errno = errns;
err_quit_0:
    delete_record(rec);
    return NULL;
}

int utils_memory_to_file(const void *filemem)
{
    if (!filemem)
        return 0;

    init_utils();

    struct utils_file_record *rec = find_record(filemem);
    if (!rec)
        return -1;

    // Address and length should be valid now
    munmap(rec->addr, rec->length);
    delete_record(rec);

    return 0;
}

#ifndef NDEBUG

#include <stdio.h>

void utils_dump_records()
{
    printf("Dumping...\n");
    if (!UTILS_INITED)
    {
        printf("utils not inited\n");
        return;
    }

    printf("head = %d, tail = %d\n", RECORDS.head, RECORDS.tail);
    for (list_size_t i = RECORDS.tail; i != RECORDS.head; i++)
    {
        struct utils_file_record *rec = RECORDS.base + i;
        printf("%3d: addr = %p, len = %lu\n", i, rec->addr, rec->length);
    }
}

#endif

