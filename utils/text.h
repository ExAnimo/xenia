
/*
 * File contains common functions to work with text.
 *
 * Note: C99 allows pointing to const-object with non-const pointers,
 * the indirection operation itself is UB. This is why some of the
 * functions on the file receive (const char *), but return just
 * (char *). strstr(3) has similar signature, for example.
 */

#include <assert.h>
#include <ctype.h>

/*
 * Returns pointer to the first non-alphanumeric or '_' charachter
 * after 'beg'.
 */
static inline char *text_get_word_end(const char *beg)
{
    assert(beg);

    while (isalnum(*beg) || *beg == '_')
        beg++;
    return (char*)beg;
}

/*
 * Returns first non-blank character after 'beg'.
 */
static inline char *text_skip_blank(const char *beg)
{
    assert(beg);

    while (*beg && isblank(*beg))
        beg++;
    return (char*)beg;
}

/*
 * Finds 'sym's position in string 'in' and strores the line on which
 * it was found in 'line' if 'line' is not NULL and the offset from
 * the line's beginning in 'linepos' if 'linepos' is not NULL.
 *
 * 'sym' is expected to point to a valid symbol in 'in' string.
 */
static inline void text_get_position(const char *in, const char *sym,
                                     size_t *line, size_t *linepos)
{
    assert(in);
    assert(sym);
    assert(sym >= in);

    size_t l = 1;
    size_t p = 1;
    while (in != sym)
        if (*in++ == '\n')
        {
            l++;
            p = 1;
        }
        else
            p++;

    if (line)
        *line = l;
    if (linepos)
        *linepos = p;
}

/*
 * Converts the string 'str' to uppercase inplace. 'str' must not be NULL.
 *
 * Returns pointer to 'str'.
 */
static inline char *text_to_upper(char *str)
{
    assert(str);

    for (char *s = str; *s; s++)
        *s = toupper(*s);
    return str;
}

