
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "vector.h"

struct vector
{
    void   *data;
    size_t  elem_size;
    size_t  capacity;
    size_t  length;
};

struct vector *
    vector_create(size_t elem_size, size_t capacity, size_t length)
{
    if (capacity < length || capacity == 0 || elem_size == 0)
        return NULL;

    struct vector *v = malloc(sizeof(*v));
    if (!v)
        return NULL;

    void *data = malloc(capacity * elem_size);
    if (!data)
    {
        free(v);
        return NULL;
    }

    v->data = data;
    v->elem_size = elem_size;
    v->capacity = capacity;
    v->length = length;

    return v;
}

int vector_resize(struct vector *v, size_t new_cap)
{
    assert(v);
    assert(v->capacity >= v->length);

    void *new_data = realloc(v->data, new_cap * v->elem_size);
    if (!new_data)
        return -1;

    v->data = new_data;
    v->capacity = new_cap;
    if (v->length > v->capacity)
        v->length = v->capacity;

    return 0;
}

int vector_append(struct vector *v, const void *new_elem)
{
    assert(v);
    assert(v->capacity >= v->length);
    assert(new_elem);

    if (v->length == v->capacity)
    {
        size_t new_cap = v->capacity * 2;
        int rc = vector_resize(v, new_cap);
        if (rc != 0)
            return -1;
    }

    /* Assuming a new element will not exceed free space */
    assert(v->capacity - v->length >= v->elem_size);

    /*
     * C99 requires sizeof(char) == 1 and sizeof(...) always yields size
     * in bytes. Thus it is safe to cast to char* to perform byte-wide
     * operations.
     *
     * It is possible to leave void* here, but void* arithmetics are not
     * permitted without GNU extensions used here.
     */
    memcpy((char*)v->data + v->length*v->elem_size, new_elem, v->elem_size);
    v->length++;

    return 0;
}

int vector_append_n(struct vector *v, const void *new_elems, size_t n_elems)
{
    assert(v);
    assert(v->capacity >= v->length);
    assert(new_elems);

    /*
     * TODO: a single memcpy should speed up the computations, but for
     * now taking the easy way.
     *
     * Do not forget about size check here!
     */

    for (size_t i = 0; i < n_elems; i++)
    {
        void *next = (char*)new_elems + i*v->elem_size;
        int rc = vector_append(v, next);
        if (rc != 0)
            return -1;
    }

    return 0;
}

void *vector_at(struct vector *v, size_t idx)
{
    assert(v);
    assert(v->capacity >= v->length);

    if (v->length <= idx)
        return NULL;

    return (char*)v->data + idx*v->elem_size;
}

size_t vector_capacity(struct vector *v)
{
    assert(v);
    assert(v->capacity >= v->length);

    return v->capacity;
}

size_t vector_length(struct vector *v)
{
    assert(v);
    assert(v->capacity >= v->length);

    return v->length;
}

void *vector_raw_data(struct vector *v)
{
    assert(v);
    assert(v->capacity >= v->length);

    return v->data;
}

void *vector_destroy(struct vector *v, int with_data)
{
    if (!v)
        return NULL;

    // Sanity check to help catching errors
    assert(v->capacity >= v->length);

    void *ret = v->data;
    if (with_data)
    {
        free(v->data);
        ret = NULL;
    }
    free(v);

    return ret;
}

