
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "str_arena.h"

struct str_arena
{
    /* Pointer to the first available char */
    char             *beg;
    /* Pointer to the char, located after the last allocated */
    char             *end;

    /* Pointer to the previous allocated chunk */
    struct str_arena *prev;
    size_t            next_chunk_size;

    char              data[];
};


static size_t max(size_t a, size_t b)
{
    return (a > b) ? a : b;
}

/*
 * Allocates chink of 'size_chunk' + sizeof(struct str_arena).
 */
static struct str_arena *allocate_chunk(size_t size_chunk)
{
    struct str_arena *arena = malloc(sizeof(*arena) + size_chunk);
    if (!arena)
        return NULL;

    arena->beg = arena->data;
    arena->end = arena->data + size_chunk;
    arena->prev = NULL;
    arena->next_chunk_size = size_chunk;

    return arena;
}

struct str_arena *str_arena_create(size_t size_hint)
{
    size_t size_chunk = max(size_hint, 1024);
    struct str_arena *body = allocate_chunk(size_chunk);
    if (!body)
        return NULL;

    /* Allocating an empty chunk as a handler */
    struct str_arena *head = allocate_chunk(0);
    if (!head)
    {
        int errns = errno;
        free(body);
        errno = errns;
        return NULL;
    }

    *head = *body;
    head->prev = body;
    body->beg = NULL;
    body->end = NULL;

    return head;
}

char *str_arena_store(struct str_arena *arena, const char *str)
{
    assert(arena);
    assert(arena->end >= arena->beg);
    assert(str);

    size_t str_size = strlen(str) + 1 /* + '\0' */;
    /*
     * Can be optimized to perform search.
     * TODO: check whether the optimization worth it
     */
    if (str_size > (size_t)(arena->end - arena->beg))
    {
        /* If not enough space left, allocate a new chunk that will fit */
        if (str_size > arena->next_chunk_size)
            arena->next_chunk_size = str_size;

        struct str_arena *new = allocate_chunk(arena->next_chunk_size);
        if (!new)
            return NULL;

        struct str_arena tmp = *new;
        *new = *arena;
        *arena = tmp;
        arena->prev = new;
    }

    char *stored_str = strcpy(arena->beg, str);
    arena->beg += str_size - 1;
    *arena->beg++ = '\0';

    return stored_str;
}

void str_arena_destroy(struct str_arena *arena)
{
    struct str_arena *prev = NULL;
    for (struct str_arena *a = arena; a; a = prev)
    {
        prev = a->prev;
        free(a);
    }
}

