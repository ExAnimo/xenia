
#include "../../utils/unittest.h"

#include "../../utils/common.h"
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

// For statics try out this:
// #include "vm.c"
#include "../vm.h"

int main()
{
    UNUSED(VM_REGNAMES);
    UNUSED(VM_FLAGNAMES);

    ut_enable_color = 1;
    ut_verbose = 0;
    ut_setup();

    UT_CHECK(int rc = vm_init(),
             UT_OK(rc == 0),
             UT_CLEANUP(vm_destroy())
            );

    UT_CHECK(struct vm_exec_img *img = vm_open_img("nonexistent"),
             UT_OK(img == NULL)
            );

    UT_CHECK(struct vm_exec_img *img = vm_open_img("broken.xex"),
             UT_OK(img == NULL),
             UT_OK(errno == ELIBSCN)
            );

    int rc;
    UT_CHECK(struct vm_exec_img *img = vm_open_img("factorial.xex"),
             UT_OK(img != NULL),
             UT_EXECUTE(vm_init()),
             UT_EXECUTE(rc = vm_load_img(img)),
             UT_OK(rc == 0),
             UT_CLEANUP((vm_destroy(), vm_close_img(img)))
            );

    ut_finalize();

    return (ut_any_failed ? EXIT_FAILURE : EXIT_SUCCESS);
}

