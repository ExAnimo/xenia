
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

#include "vm.h"
#include "exec_img.h"
#include "../utils/common.h"
#include "../utils/log.h"

/*
 * The global instance of VM.
 */
struct vm VM;

// Functions to use in "native_asm.h"   =======================
/*
 * Sets the 'flag''s bit to 'value'.
 *
 * Returns the old mask.
 */
static vm_flags_t vm_setflag(int flag, int value)
{
    assert(value == 0 || value == 1);
    assert(flag >= 0);

    vm_flags_t old = VM.flags;
    // Erase the old value
    VM.flags &= (~(1 << flag));
    VM.flags |= (value << flag);

    return old;
}

/*
 * Sets the 'flag''s bit to 'value'.
 *
 * Returns the flag's value.
 */
static int vm_getflag(int flag)
{
    assert(flag >= 0);

    return (VM.flags & (1 << flag)) != 0;
}

/*
 * Returns 1 if 'addr' is a valid address (fits in memory),
 * 0 otherwise.
 */
static int vm_addr_in_mem(vm_opcode_t addr)
{
    return (addr >= 0 && addr < MEMSIZE);
}

/*
 * Returns 1 when a memory-bound register 'regptr' (i.e. rip)
 * points to a valid address, 0 otherwise.
 */
static int vm_regptr_in_mem(vm_opcode_t *regptr)
{
    return (regptr >= VM.memory && regptr < VM.memory + MEMSIZE);
}

#ifndef   NDEBUG
static void vm_dump(const struct vm *vm)
{
    assert(vm);

    ptrdiff_t rip = vm->rip - vm->memory;
    fprintf(stderr, "RIP = %8tu (%#8tx)\n", rip, rip);

    for (int reg = RAX; reg != NREGS; reg++)
    {
        vm_opcode_t reg_data = vm->reg[reg];
        fprintf(stderr, "%s = %8"PRIvmopcode" (%#8"PRIvmopcodex")\n",
            VM_REGNAMES[reg], reg_data, reg_data);
    }

    for (int flag = ZF; flag != NFLAGS; flag++)
        fprintf(stderr, "%s = %d\n", VM_FLAGNAMES[flag], vm_getflag(flag));
}
#endif // NDEBUG

// VM code  ====================================================

struct vm_exec_img
{
    struct vm_exec_img_hdr  header;
    FILE                   *file;
};

/*
 * Checks header consistency and whether the VM can run it. Sets errno as
 * vm_open_img() should do it.
 */
static int check_hdr(struct vm_exec_img_hdr *hdr)
{
    assert(hdr);

    // Format check
    if (hdr->magic != VM_MAGIC || hdr->version > VM_VERSION)
    {
        errno = ELIBSCN;
        return -1;
    }

    // Sections overlap
    if (hdr->code_beg <= hdr->data_beg &&
        hdr->code_beg +  hdr->code_len > hdr->data_beg)
    {
        errno = ELIBSCN;
        return -1;
    }
    if (hdr->data_beg <= hdr->code_beg &&
        hdr->data_beg +  hdr->data_len > hdr->code_beg)
    {
        errno = ELIBSCN;
        return -1;
    }
    if (hdr->code_mem <= hdr->data_mem &&
        hdr->code_mem +  hdr->code_len > hdr->data_mem)
    {
        errno = E2BIG;
        return -1;
    }
    if (hdr->data_mem <= hdr->code_mem &&
        hdr->data_mem +  hdr->data_len > hdr->code_mem)
    {
        errno = E2BIG;
        return -1;
    }

    // Not enough memory
    if (hdr->code_mem +  hdr->code_len > MEMSIZE ||
        hdr->data_mem +  hdr->data_len > MEMSIZE)
    {
        errno = E2BIG;
        return -1;
    }

    return 0;
}

struct vm_exec_img *vm_open_img(const char *filename)
{
    assert(filename);

    size_t rc;
    int errns = 0;

    FILE *infile = fopen(filename, "r");
    if (!infile)
        goto err_quit_0;

    struct vm_exec_img *img = malloc(sizeof(*img));
    if (!img)
    {
        errns = errno;
        goto err_quit_1;
    }

    rc = fread(&img->header, sizeof(img->header), 1, infile);
    if (rc != 1)
    {
        if (feof(infile))
            errno = ELIBSCN;
        errns = errno;
        goto err_quit_2;
    }

    LOG("Checking header... ");
    rc = check_hdr(&img->header);
    if (rc)
        goto err_quit_2;
    LOG("OK\n");

    img->file = infile;
    return img;

err_quit_2:
    LOG("FAILED\n");
    free(img);
err_quit_1:
    fclose(infile);
    errno = errns;
err_quit_0:
    return NULL;
}

void vm_close_img(struct vm_exec_img *img)
{
    assert(img);

    fclose(img->file);
    free(img);
}

int vm_init()
{
    VM.rip = NULL;
    VM.device_in = stdin;
    VM.device_out = stdout;
    memset(VM.reg, 0, sizeof(VM.reg));
    VM.memory = malloc(sizeof(VM.memory[0]) * MEMSIZE);
    return -(VM.memory == NULL);
}

vm_dev_in  vm_plug_input(vm_dev_in device_in)
{
    vm_dev_in old = VM.device_in;
    VM.device_in = device_in;
    return old;
}

vm_dev_out vm_plug_output(vm_dev_out device_out)
{
    vm_dev_out old = VM.device_out;
    VM.device_out = device_out;
    return old;
}

void vm_destroy()
{
    // Can be placed to any function
    UNUSED(VM_REGNAMES);
    UNUSED(VM_FLAGNAMES);
    UNUSED(vm_addr_in_mem);
    UNUSED(vm_regptr_in_mem);

    free(VM.memory);
    VM.memory = NULL;
}

int  vm_load_img(struct vm_exec_img *img)
{
    assert(img);

    int rc;
    struct vm_exec_img_hdr *hdr = &img->header;

    LOG("Loading image... ");
    // Read img into memory
    rc = fseek(img->file, hdr->code_beg, SEEK_SET);
    if (rc == -1)
        return -1;

    rc = fread(VM.memory + hdr->code_mem, 1, hdr->code_len, img->file);
    if (rc < (int)hdr->code_len)    // Assume code is relatively short
    {
        if (feof(img->file))
            errno = ELIBSCN;
        return -1;
    }

    rc = fseek(img->file, hdr->data_beg, SEEK_SET);
    if (rc == -1)
        return -1;

    rc = fread(VM.memory + hdr->data_mem, 1, hdr->data_len, img->file);
    if (rc < (int)hdr->data_len)    // Assume code is relatively short
    {
        if (feof(img->file))
            errno = ELIBSCN;
        return -1;
    }

    // Set registers
    VM.rip = VM.memory + hdr->code_mem;
    VM.reg[RSP] = MEMSIZE - 1;
    VM.reg[RBP] = MEMSIZE - 1;

    LOG("OK\n");
    return 0;
}

int  vm_step()
{
    struct vm *vm = &VM;    // Another interface to the VM
    switch (*VM.rip)
    {
        /*
         * CODE GENERATION
         */
        #include "vm_execute_case.h"

        default:  return -1;
    }
    return 0;
}

int  vm_run(int interactive)
{
    int rc;
    LOG_CALL(vm_dump(&VM));
    while ((rc = vm_step()) == 0)
        if (interactive)
            getchar();
    return (rc == 1) ? 0 : -1;
}

// =================================================================
// Simplified API - handles error-printing
/*
 * Delegate error printing to special functions with a suffix '_easy'.
 */

/*
 * Calls vm_open_img('filename') and prints a descriptive error message to
 * 'errout' in case of failure.
 *
 * Returns opened image in case of success, prints error message and returns
 * NULL in case of error.
 */
static struct vm_exec_img *
    vm_open_img_easy(const char *filename, FILE *errout)
{
    assert(filename);
    assert(errout);

    struct vm_exec_img *img = vm_open_img(filename);
    if (img)
        return img;

    switch (errno)
    {
        case E2BIG:
            fprintf(errout, "vm_open_img_easy(): "
                            "File is too bit for the VM\n");
            break;
        case ELIBSCN:
            fprintf(errout, "vm_open_img_easy(): "
                            "File or file header is corrupted\n");
            break;
        default:
            fprintf(errout, "vm_open_img(): %s\n", strerror(errno));
            break;
    }
    return NULL;
}

/*
 * Calls vm_load_img('img') and prints a descriptive error message to
 * 'errout' in case of failure.
 *
 * Returns 0 in case of success, prints error message and returns
 * (-1) in case of error.
 */
static int vm_load_img_easy(struct vm_exec_img *img, FILE *errout)
{
    assert(img);

    int rc = vm_load_img(img);
    if (rc == 0)
        return 0;

    switch (rc)
    {
        case ELIBSCN:
            fprintf(errout, "vm_load_img_easy():"
                            "File or file header is corrupted\n");
            break;
        default:
            fprintf(errout, "vm_load_img(): %s\n", strerror(errno));
            break;
    }
    return -1;
}

int vm_setup(const char *execfile, FILE *errout)
{
    assert(execfile);
    assert(errout);

    int rc;

    struct vm_exec_img *img = vm_open_img_easy(execfile, errout);
    if (!img)
        return -1;

    rc = vm_init();
    if (rc != 0)
    {
        fprintf(errout, "vm_init(): %s\n", strerror(errno));
        goto err_quit_0;
    }

    rc = vm_load_img_easy(img, errout);
    if (rc != 0)
        goto err_quit_1;

    vm_close_img(img);
    return 0;

err_quit_1:
    vm_destroy();
err_quit_0:
    vm_close_img(img);
    return -1;
}

void vm_shutdown()
{
    vm_destroy();
}

