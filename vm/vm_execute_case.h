
/*
 * This file generates the switch for the VM according to the
 * description found in 'natice_asm.h'. Pay attention to the usage of
 * this file: it is only included once in vm.c and generates cases for
 * the switch to execute asm commands.
 */

#ifndef   VM_EXECUTE_STEPS_H_INCLUDED
#define   VM_EXECUTE_STEPS_H_INCLUDED

#define CMD(name, suff, asm_code, vm_code)                              \
    case name##suff :                                                   \
    {                                                                   \
        LOG("> %s:\n", STR(name##suff));                                \
        vm_code;                                                        \
        LOG_CALL(vm_dump(vm));                                          \
        break;                                                          \
    }

/*
 * Generating cases for each instruction
 */
#include "native_asm.h"

/*
 * Cleanup
 */
#undef CMD

#endif // VM_EXECUTE_STEPS_H_INCLUDED
