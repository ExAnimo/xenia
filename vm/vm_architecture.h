
/*
 * File contains declarations for VM.
 *
 * There is only one instance of VM can be used at a time since there is
 * no real reason to maintain several VM instances in a single process.
 * Thus a single global instance called VM is used.
 */

#ifndef   VM_ARCHITECTURE
#define   VM_ARCHITECTURE

#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stddef.h>

/*
 * The type for assembly commands and its arguments for external use.
 *
 * Note: cast from vm_opcode_t to vm_uopcode_t is safe and well-defined,
 * the opposite is not nesessarily true. Thus it is better to store
 * the opcodes in signed version and cast to unsigned when needed.
 */
typedef int64_t vm_opcode_t;
typedef uint64_t vm_uopcode_t;
#define PRIvmopcode PRId64
#define SCNvmopcode SCNd64
#define PRIuvmopcode PRIu64
#define SCNuvmopcode SCNu64
#define PRIvmopcodex PRIx64
#define SCNvmopcodex SCNx64

/*
 * 'Processor' flags type.
 */
typedef uint64_t vm_flags_t;

/*
 * IO-device types.
 * Probably, will never be changed to other types but it is still
 * possible...
 */
typedef FILE* vm_dev_in;
typedef FILE* vm_dev_out;

/*
 * CODE GENERATION
 */
#define CMD(name, suff, asm_code, vm_code) name##suff ,
enum vm_opcodes
{
    #include "native_asm.h"
    N_OPCODES
};
#undef CMD

/*
 * Enum values are used for addressing in the array of regular regs,
 * e.g. reg[RAX].
 *
 * Could have used include-code-generation here but it would be overkill.
 */
enum vm_registers
{
    RAX, RBX, RCX, RDX,
    R01, R02, R03, R04,
    RBP, RSP,

    NREGS
};
static const char *VM_REGNAMES[] =
{
    "RAX", "RBX", "RCX", "RDX",
    "R01", "R02", "R03", "R04",
    "RBP", "RSP",

    NULL
};

/*
 * Memory size attributes. Hard-coded for now.
 */
enum vm_memory
{
    MEMSIZE = 1024*1024 // Megabyte
};

/*
 * Operation flags
 */
enum vm_flags
{
    ZF, // Zero flag - JE,JZ (ZF = 1) / JNE,JNZ (ZF = 0)
    SLF, // Signed less flag - set when left operand is less than the right
    ULF, // Unsigned less flag - set when left operand is less than the right

    NFLAGS
};

static const char *VM_FLAGNAMES[] =
{
    "ZF",
    "SLF",
    "ULF",

    NULL
};

/*
 * The structure representing a Virtual Machine: some approximation of a
 * modern computer. Consists of a "processor" of x86-like architechture
 * and memory. No address translations happen, all the addresses are
 * considered absolute. Instructions and data are stored in the same
 * memory.
 */
struct vm
{
    // Pointer to instruction
    vm_opcode_t *rip;

    // "Registers"
    vm_opcode_t  reg[NREGS];

    // "Memory". Expected to be memory-mapped file (at least for now)
    vm_opcode_t *memory;

    vm_flags_t   flags;

    // IO
    vm_dev_in    device_in;
    vm_dev_out   device_out;
};

#endif // VM_ARCHITECTURE

