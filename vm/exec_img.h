
/*
 * File contains executable file format for VM.
 */

#ifndef   EXEC_IMG_H_INCLUDED
#define   EXEC_IMG_H_INCLUDED

#include <stdint.h>

/*
 * The structure of the headr for the executable for VM. Fields have the
 * following meaning:
 *
 *  - magic:    magic number for the executable. See VM_MAGIC constant;
 *  - version:  version of the file;
 *  - other:    reserved for future usage;
 *
 *      Following fields are used for memory mapping:
 *
 *  - code_mem: code initial address when loaded in memory, RIP initial value
 *  - code_beg: code part offset in the file;
 *  - code_len: length of the code part;
 *  - data_mem: data initial address when loaded in memory;
 *  - data_beg: data part offset in the file;
 *  - data_len: length of the data part;
 *
 * TODO: do not take endianness into account now.
 */
struct vm_exec_img_hdr
{
    uint32_t    magic;
    uint8_t     version;
    uint8_t     other[3];

    size_t      code_mem;
    size_t      code_beg;
    size_t      code_len;
    size_t      data_mem;
    size_t      data_beg;
    size_t      data_len;
};

#define VM_MAGIC   0x1234
#define VM_VERSION 1

#endif // EXEC_IMG_H_INCLUDED

