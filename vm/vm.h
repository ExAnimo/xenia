
/*
 * File contains API declarations for VM.
 */

#ifndef   VM_H_INCLUDED
#define   VM_H_INCLUDED

#include "vm_architecture.h"

/*
 * Structure representing executable.
 */
struct vm_exec_img;

// Functions =======================================================

/*
 * Reads 'filename' and prepares image-description structure. Checks whether
 * the 'filename' exists and contains correct header. Call vm_free_img()
 * before exit.
 *
 * Returns NULL in case of failure and sets errno appropriately,
 * filled struct otherwise. Errno can be set by malloc, fopen or
 *  E2BIG   - file cannot be fit into VM's memory;
 *  ELIBSCN - file or file header is corrupted.
 */
struct vm_exec_img *
     vm_open_img(const char *filename);

/*
 * Deallocates resourses.
 */
void vm_close_img(struct vm_exec_img *img);

/*
 * Initializes the global struct VM. No executable is assotiated with the
 * struct: vm_load_executable() should be called. Call vm_destroy() before
 * exit.
 *
 * Returns (-1) in case of failure and sets errno appropriately, 0 otherwise.
 * Errno is set by malloc.
 */
int  vm_init();

/*
 * Assosiate an IO device with the VM.
 *
 * Returns the old device, no errors.
 */
vm_dev_in  vm_plug_input(vm_dev_in device_in);
vm_dev_out vm_plug_output(vm_dev_out device_out);

/*
 * Deallocates resourses.
 */
void vm_destroy();

/*
 * Associates the executable 'img' with the virtual machine and prepares
 * it to run: sets registers, stack, checks file format.
 *
 * Returns (-1) in case of failure and sets errno appropriately, 0 otherwise.
 * Errno is set by fseek or fread or
 *  ELIBSCN - file or file header is corrupted.
 */
int  vm_load_img(struct vm_exec_img *img);

/*
 * Executes the command RIP points to.
 *
 * Returns (-1) in case of illegal instruction, 0 otherwise.
 */
int  vm_step();

/*
 * Executes the whole assosiated file.
 * If 'interactive' != 0, stop after each step and print executed command.
 *
 * Returns (-1) in case of illegal instruction, 0 otherwise.
 */
int  vm_run(int interactive);

// =================================================================
// Simplified API - handles error-printing and data flow

/*
 * Opens an 'execfile', assosiates it with the VM and prepares to run.
 * Prints descriptive messages to 'errout' in case of errors.
 * Call 'vm_shutdown()' only after the call to 'vm_setup()'.
 *
 * Returns (-1) in case of error, 0 otherwise.
 */
int vm_setup(const char *execfile, FILE *errout);

/*
 * Destroys the VM, frees all the resources.
 */
void vm_shutdown();

#endif // VM_H_INCLUDED

