
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

/*
 * TODO: Non-portable
 */
#include <getopt.h>

#include "../utils/common.h"
#include "vm.h"

static void help(FILE *out)
{
    assert(out);

    fprintf(out,
        "Options:\n"
        "   -i  enable interactive execution\n"
        "       press ENTER to execute next step\n"
        "   -h  print this message and exit\n"
        );
}

static void usage(FILE *out, const char *name)
{
    assert(out);
    assert(name);

    fprintf(out, "usage: %s [-i][-h] executable.xex\n", name);
}

int main(int argc, char **argv)
{
    // Fixing warning...
    UNUSED(VM_REGNAMES);
    UNUSED(VM_FLAGNAMES);

    int ret = EXIT_SUCCESS;

    int opt;
    int interactive = 0;
    opterr = 0; // Suppressing error output
    while ((opt = getopt(argc, argv, "ih")) != -1)
        switch (opt)
        {
            case 'i':
                interactive = 1;
                break;
            case 'h':
                help(stdout);
                return EXIT_SUCCESS;
            default:
                usage(stderr, argv[0]);
                return EXIT_FAILURE;
        }
    if (optind != (argc - 1))
    {
        usage(stderr, argv[0]);
        return EXIT_FAILURE;
    }

    // Finally, all is OK
    int rc = vm_setup(argv[optind], stderr);
    if (rc != 0)
        return EXIT_FAILURE;

    rc = vm_run(interactive);
    if (rc != 0)
    {
        fprintf(stderr, "vm_run() failed\n");
        ret = EXIT_FAILURE;
    }

    vm_shutdown();

    return ret;
}

