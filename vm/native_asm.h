
/*
 * TODO: vm->rip should be moved automatically
 *
 * ATTENTION: this file is used for code generation in many files.
 * Pay significant attention to its contents and run tests after
 * any change to the contents.
 *
 * To use it, define marco CMD with appropriate number of arguments
 * in your file and then include this file. Do not forget to undef
 * the macro after usage or you may occasionally run into hard-to-debug
 * troubles. If you ultimately got such problems, try 'gcc -E'. To
 * get more clear output try 'gcc -E | grep -v '^#' | vim -'.
 *
 * Format: CMD (NAME, SUFFIX, XASM_CODE, VM_CODE)
 *  - NAME:         name of the asm command that is used in asm code;
 *  - SUFFIX:       optional field to distinguish different semantics,
 *                  e.g. "ADD RBX, RAX" and "ADD RAX, [1024]";
 *  - XASM_CODE:    the code describing command argument types, such as
 *                  a register or a memory. Currently supported variants
 *                  are USE(reg) and USE(num).
 *  - VM_CODE:      code for VM. Should use the pointer to a struct VM
 *                  instance called 'vm' or a struct VM instance 'VM'.
 *                  Do not return from the code.
 *
 * Just ignore the fields that you do not need.
 *
 * Do not put semicolon after CMD() in this file!
 */

// Add
CMD(ADD, _REG_REG,
    {
        USE(reg)
        USE(reg)
    }
    ,
    {
        vm_opcode_t src = *(vm->rip + 1);
        vm_opcode_t dst = *(vm->rip + 2);
        vm->reg[dst] += vm->reg[src];
        vm->rip += 3;
    })

// Move
CMD(MOV, _REG_REG,
    {
        USE(reg)
        USE(reg)
    }
    ,
    {
        vm_opcode_t src = *(vm->rip + 1);
        vm_opcode_t dst = *(vm->rip + 2);
        vm->reg[dst] = vm->reg[src];
        vm->rip += 3;
    })
CMD(MOV, _NUM_REG,
    {
        USE(num)
        USE(reg)
    }
    ,
    {
        vm_opcode_t num = *(vm->rip + 1);
        vm_opcode_t dst = *(vm->rip + 2);
        vm->reg[dst] = num;
        vm->rip += 3;
    })
CMD(MOV, _MEM_NUM_REG,
    {
        USE(num)
        USE(reg)
    }
    ,
    {
        vm_opcode_t num = *(vm->rip + 1);
        vm_opcode_t dst = *(vm->rip + 2);
        vm->reg[dst] = vm->memory[num];
        vm->rip += 3;
    })

// Execution flow control
CMD(CALL, _LBL,
    {
        USE(num)
    }
    ,
    {
        vm_opcode_t dst = *(vm->rip + 1);
        assert(vm_addr_in_mem(dst));
        // Save address of the next instruction
        vm->reg[RSP] -= 1;
        vm->memory[vm->reg[RSP]] = (vm->rip - vm->memory) + 2;
        vm->rip = vm->memory + dst;
    })
CMD(RET, ,
    {},
    {
        vm->rip = vm->memory + vm->memory[vm->reg[RSP]];
        vm->reg[RSP] += 1;
    })
CMD(JMP, _LBL,
    {
        USE(num)
    }
    ,
    {
        vm_opcode_t dst = *(vm->rip + 1);
        assert(vm_addr_in_mem(dst));
        vm->rip = vm->memory + dst;
    })
CMD(CMP, _REG_REG,
    {
        USE(reg)
        USE(reg)
    }
    ,
    {
        vm_opcode_t r1 = *(vm->rip + 1);
        vm_opcode_t r2 = *(vm->rip + 2);
        vm_setflag(ZF,  (vm->reg[r1] == vm->reg[r2]));
        vm_setflag(SLF, (vm->reg[r1] <  vm->reg[r2]));
        vm_setflag(ULF,
            ((vm_uopcode_t)vm->reg[r1] < (vm_uopcode_t)vm->reg[r2]));
        vm->rip += 3;
    })
CMD(JE, _LBL,
    {
        USE(num)
    }
    ,
    {
        vm_opcode_t dst = *(vm->rip + 1);
        if (vm_getflag(ZF))
        {
            assert(vm_addr_in_mem(dst));
            vm->rip = vm->memory + dst;
        }
        else
            vm->rip += 2;
    })
CMD(JZ, _LBL,
    {
        USE(num)
    }
    ,
    {
        vm_opcode_t dst = *(vm->rip + 1);
        if (vm_getflag(ZF))
        {
            assert(vm_addr_in_mem(dst));
            vm->rip = vm->memory + dst;
        }
        else
            vm->rip += 2;
    })
CMD(JL, _LBL,
    {
        USE(num)
    }
    ,
    {
        vm_opcode_t dst = *(vm->rip + 1);
        if (vm_getflag(SLF))
        {
            assert(vm_addr_in_mem(dst));
            vm->rip = vm->memory + dst;
        }
        else
            vm->rip += 2;
    })
CMD(JLE, _LBL,
    {
        USE(num)
    }
    ,
    {
        vm_opcode_t dst = *(vm->rip + 1);
        if (vm_getflag(SLF) || vm_getflag(ZF))
        {
            assert(vm_addr_in_mem(dst));
            vm->rip = vm->memory + dst;
        }
        else
            vm->rip += 2;
    })
CMD(JG, _LBL,
    {
        USE(num)
    }
    ,
    {
        vm_opcode_t dst = *(vm->rip + 1);
        if (!vm_getflag(SLF) && !vm_getflag(ZF))
        {
            assert(vm_addr_in_mem(dst));
            vm->rip = vm->memory + dst;
        }
        else
            vm->rip += 2;
    })
CMD(JGE, _LBL,
    {
        USE(num)
    }
    ,
    {
        vm_opcode_t dst = *(vm->rip + 1);
        if (!vm_getflag(SLF))
        {
            assert(vm_addr_in_mem(dst));
            vm->rip = vm->memory + dst;
        }
        else
            vm->rip += 2;
    })
CMD(JB, _LBL,
    {
        USE(num)
    }
    ,
    {
        vm_opcode_t dst = *(vm->rip + 1);
        if (vm_getflag(ULF))
        {
            assert(vm_addr_in_mem(dst));
            vm->rip = vm->memory + dst;
        }
        else
            vm->rip += 2;
    })
CMD(JBE, _LBL,
    {
        USE(num)
    }
    ,
    {
        vm_opcode_t dst = *(vm->rip + 1);
        if (vm_getflag(ULF) || vm_getflag(ZF))
        {
            assert(vm_addr_in_mem(dst));
            vm->rip = vm->memory + dst;
        }
        else
            vm->rip += 2;
    })
CMD(JA, _LBL,
    {
        USE(num)
    }
    ,
    {
        vm_opcode_t dst = *(vm->rip + 1);
        if (!vm_getflag(ULF) && !vm_getflag(ZF))
        {
            assert(vm_addr_in_mem(dst));
            vm->rip = vm->memory + dst;
        }
        else
            vm->rip += 2;
    })
CMD(JAE, _LBL,
    {
        USE(num)
    }
    ,
    {
        vm_opcode_t dst = *(vm->rip + 1);
        if (!vm_getflag(ULF))
        {
            assert(vm_addr_in_mem(dst));
            vm->rip = vm->memory + dst;
        }
        else
            vm->rip += 2;
    })
CMD(JNE, _LBL,
    {
        USE(num)
    }
    ,
    {
        vm_opcode_t dst = *(vm->rip + 1);
        if (!vm_getflag(ZF))
        {
            assert(vm_addr_in_mem(dst));
            vm->rip = vm->memory + dst;
        }
        else
            vm->rip += 2;
    })
CMD(JNZ, _LBL,
    {
        USE(num)
    }
    ,
    {
        vm_opcode_t dst = *(vm->rip + 1);
        if (!vm_getflag(ZF))
        {
            assert(vm_addr_in_mem(dst));
            vm->rip = vm->memory + dst;
        }
        else
            vm->rip += 2;
    })
CMD(JNL, _LBL,
    {
        USE(num)
    }
    ,
    {
        vm_opcode_t dst = *(vm->rip + 1);
        if (!vm_getflag(SLF))
        {
            assert(vm_addr_in_mem(dst));
            vm->rip = vm->memory + dst;
        }
        else
            vm->rip += 2;
    })
CMD(JNLE, _LBL,
    {
        USE(num)
    }
    ,
    {
        vm_opcode_t dst = *(vm->rip + 1);
        if (!vm_getflag(SLF) && !vm_getflag(ZF))
        {
            assert(vm_addr_in_mem(dst));
            vm->rip = vm->memory + dst;
        }
        else
            vm->rip += 2;
    })
CMD(JNG, _LBL,
    {
        USE(num)
    }
    ,
    {
        vm_opcode_t dst = *(vm->rip + 1);
        if (vm_getflag(SLF) || vm_getflag(ZF))
        {
            assert(vm_addr_in_mem(dst));
            vm->rip = vm->memory + dst;
        }
        else
            vm->rip += 2;
    })
CMD(JNGE, _LBL,
    {
        USE(num)
    }
    ,
    {
        vm_opcode_t dst = *(vm->rip + 1);
        if (vm_getflag(SLF))
        {
            assert(vm_addr_in_mem(dst));
            vm->rip = vm->memory + dst;
        }
        else
            vm->rip += 2;
    })
CMD(JNB, _LBL,
    {
        USE(num)
    }
    ,
    {
        vm_opcode_t dst = *(vm->rip + 1);
        if (!vm_getflag(ULF))
        {
            assert(vm_addr_in_mem(dst));
            vm->rip = vm->memory + dst;
        }
        else
            vm->rip += 2;
    })
CMD(JNBE, _LBL,
    {
        USE(num)
    }
    ,
    {
        vm_opcode_t dst = *(vm->rip + 1);
        if (!vm_getflag(ULF) && !vm_getflag(ZF))
        {
            assert(vm_addr_in_mem(dst));
            vm->rip = vm->memory + dst;
        }
        else
            vm->rip += 2;
    })
CMD(JNA, _LBL,
    {
        USE(num)
    }
    ,
    {
        vm_opcode_t dst = *(vm->rip + 1);
        if (vm_getflag(ULF) || vm_getflag(ZF))
        {
            assert(vm_addr_in_mem(dst));
            vm->rip = vm->memory + dst;
        }
        else
            vm->rip += 2;
    })
CMD(JNAE, _LBL,
    {
        USE(num)
    }
    ,
    {
        vm_opcode_t dst = *(vm->rip + 1);
        if (vm_getflag(ULF))
        {
            assert(vm_addr_in_mem(dst));
            vm->rip = vm->memory + dst;
        }
        else
            vm->rip += 2;
    })

// Stack ops
CMD(PUSH, _REG,
    {
        USE(reg)
    }
    ,
    {
        if (!vm_addr_in_mem(vm->reg[RSP]))
            return -1;

        vm_opcode_t reg = *(vm->rip + 1);
        vm->reg[RSP] -= 1;
        vm->memory[vm->reg[RSP]] = vm->reg[reg];
        vm->rip += 2;
    })
CMD(POP, _REG,
    {
        USE(reg)
    }
    ,
    {
        if (!vm_addr_in_mem(vm->reg[RSP]))
            return -1;

        vm_opcode_t reg = *(vm->rip + 1);
        vm->reg[reg] = vm->memory[vm->reg[RSP]];
        vm->reg[RSP] += 1;
        vm->rip += 2;
    })

// Arithmetics
CMD(INC, _REG,
    {
        USE(reg)
    }
    ,
    {
        vm_opcode_t reg = *(vm->rip + 1);
        vm->reg[reg]++;
        vm->rip += 2;
    })
CMD(DEC, _REG,
    {
        USE(reg)
    }
    ,
    {
        vm_opcode_t reg = *(vm->rip + 1);
        vm->reg[reg]--;
        vm->rip += 2;
    })
CMD(SUM, _REG_REG,
    {
        USE(reg)
        USE(reg)
    }
    ,
    {
        vm_opcode_t r1 = *(vm->rip + 1);
        vm_opcode_t r2 = *(vm->rip + 2);
        vm->reg[RAX] = vm->reg[r1] + vm->reg[r2];
        vm->rip += 3;
    })
CMD(SUB, _REG_REG,
    {
        USE(reg)
        USE(reg)
    }
    ,
    {
        vm_opcode_t r1 = *(vm->rip + 1);
        vm_opcode_t r2 = *(vm->rip + 2);
        vm->reg[RAX] = vm->reg[r1] - vm->reg[r2];
        vm->rip += 3;
    })
CMD(MUL, _REG_REG,
    {
        USE(reg)
        USE(reg)
    }
    ,
    {
        vm_opcode_t r1 = *(vm->rip + 1);
        vm_opcode_t r2 = *(vm->rip + 2);
        vm->reg[RAX] = vm->reg[r1] * vm->reg[r2];
        vm->rip += 3;
    })
CMD(DIV, _REG_REG,
    {
        USE(reg)
        USE(reg)
    }
    ,
    {
        vm_opcode_t r1 = *(vm->rip + 1);
        vm_opcode_t r2 = *(vm->rip + 2);
        vm_opcode_t rax = vm->reg[r1] / vm->reg[r2];
        vm_opcode_t rdx = vm->reg[r1] % vm->reg[r2];
        vm->reg[RAX] = rax;
        vm->reg[RDX] = rdx;
        vm->rip += 3;
    })

// Others
CMD(OUT, _REG,
    {
        USE(reg)
    }
    ,
    {
        vm_opcode_t reg = *(vm->rip + 1);
        vm->rip += 2;
        fprintf(vm->device_out, "xvm: %"PRIvmopcode"\n", vm->reg[reg]);
    })
CMD(OUT, _NUM,
    {
        USE(num)
    }
    ,
    {
        vm_opcode_t num = *(vm->rip + 1);
        vm->rip += 2;
        fprintf(vm->device_out, "xvm: %"PRIvmopcode"\n", num);
    })
CMD(END, ,
    {},
    {
        return 1;
    })

